from aleclust.globals import STATIC_CDA, PARAMETERS


class Parameters:
    def __init__(self, graph_type=None, bipartite=True, projection=True, projection_set=None,
                 source_entity_type='Hyperedge', target_entity_type='Node', time_slot_key="ts", nodes_key="nodes",
                 links_key="links", metrics=None, clustering_algo=None, algorithm_list=STATIC_CDA):
        # Default parameters values
        self.graph_type = graph_type
        self.bipartite = bipartite
        self.projection = projection
        self.projection_set = projection_set
        if projection_set is None:
            self.projection_set = target_entity_type

        self.source_entity_type = source_entity_type
        self.target_entity_type = target_entity_type
        self.time_slot_key = time_slot_key
        self.nodes_key = nodes_key
        self.links_key = links_key
        self.metrics = metrics
        self.communities = None

        self.clustering_algo = clustering_algo
        self.algorithm_list = algorithm_list

        # Parameters from the request URL and not json metadata
        self.clustering_parameters = {}
        self.preprocessing_parameters = {}

    def parse_meta_data(self, json_metadata):
        if "graph_type" in json_metadata:
            self.graph_type = json_metadata["graph_type"]
        if "source_entity_type" in json_metadata:
            self.source_entity_type = json_metadata["source_entity_type"]
        if "target_entity_type" in json_metadata:
            self.target_entity_type = json_metadata["target_entity_type"]
        if "time_slot" in json_metadata:
            self.time_slot_key = json_metadata["time_slot"]
        if "nodes" in json_metadata:
            self.nodes_key = json_metadata["nodes"]
        if "links" in json_metadata:
            self.links_key = json_metadata["links"]

        if "community" in json_metadata:
            self.communities = json_metadata["community"]

    @staticmethod
    def args_to_dict(url_args):
        """
        Take the immutable dict of the url parameters and transform it into a dictionnary. Transform the multiple arguments into a list : ?metrics=coverage&metrics=performance will give {metrics : ["coverage", "performance"]}
        """
        args_dict = {}
        for arg in url_args:
            value = url_args.getlist(arg)
            if (len(
                    value) <= 1 and arg != "metrics" and arg != "algorithms" and arg != 'consolidated_nodes' and arg != 'algorithm_list') or \
                    value[0] == 'None':  # even if there is only one metric argument, it needs to be in a list
                value = value[0]
            args_dict[arg] = value
        return args_dict

    def parse_parameters(self, clustering_algo, args_url=None):
        # Order is important
        self.set_default_clustering_parameters(clustering_algo)
        if args_url is not None:
            self.parse_args(args_url, clustering_algo)
        self.parse_preprocessing_parameters()

    def parse_args(self, args_url, clustering_algo):
        """
        Parse the arguments in the URL : classify them as clustering or preprocessing parameters and convert them to 
        their types specified in the parameters json.
        """
        # Dict of args. Args with several values have a list of values
        args_dict = self.args_to_dict(args_url)

        for arg in args_dict:
            # Because bool("False") = True
            if args_dict[arg] == "True":
                args_dict[arg] = True
            elif args_dict[arg] == "False":
                args_dict[arg] = False

            # If value is "None", convert to NoneType
            if arg in self.algorithm_list[clustering_algo]["parameters"] and self.algorithm_list[clustering_algo][
                "parameters"] != "None":
                if args_dict[arg] == "None":
                    self.clustering_parameters[arg] = None
                else:
                    self.clustering_parameters[arg] = self.algorithm_list[clustering_algo]["parameters"][arg]["type"](
                        args_dict[arg])
            elif arg in PARAMETERS.keys():
                if args_dict[arg] == "None":
                    self.preprocessing_parameters[arg] = None
                else:
                    print(arg)
                    self.preprocessing_parameters[arg] = PARAMETERS[arg]["type"](args_dict[arg])

    def set_default_clustering_parameters(self, clustering_algo):
        self.clustering_parameters = {}
        if not (self.algorithm_list[clustering_algo]["parameters"] in ("None", None)):
            for parameter in self.algorithm_list[clustering_algo]["parameters"]:
                if parameter not in self.clustering_parameters:

                    # Some clustering parameters correspond to preprocessing parameters, for bipartite graphs
                    if parameter == "source_entity_type":
                        self.clustering_parameters[parameter] = self.source_entity_type
                    elif parameter == "target_entity_type":
                        self.clustering_parameters[parameter] = self.target_entity_type
                    else:
                        # Default behaviour
                        self.clustering_parameters[parameter] = self.algorithm_list[clustering_algo]['parameters'][
                            parameter]["default"]
        return self.clustering_parameters

    def parse_preprocessing_parameters(self):
        if "projection" in self.preprocessing_parameters:
            self.projection = self.preprocessing_parameters["projection"]

        if "projection_set" in self.preprocessing_parameters:
            self.projection_set = self.preprocessing_parameters["projection_set"]
        else:
            self.projection_set = self.target_entity_type

        if "metrics" in self.preprocessing_parameters:
            self.metrics = self.preprocessing_parameters["metrics"]

        if self.projection:
            self.bipartite = False
        else:
            self.bipartite = True
