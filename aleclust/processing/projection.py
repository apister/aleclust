#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 21 10:50:25 2020

@author: alexis
"""

import networkx as nx
import itertools


# TODO : directly on nx graph
def weighted_projection(bipartite_json, projection_set="people", secondary_set="publication", is_time=True,
                        time_key="ts", weight_key="w", attribute_to_save=None, return_json=False):
    """project a bipartite graph in json format in the projection set. Use networkx for the projection. time_key is
    the time key name used by the bipartite_json and will be used for the new unipartite graph. weight_key will be
    the key name used for the weight in the new graph. """
    # unipartite graph
    G_new = nx.Graph()

    # convert to nx graph
    if type(bipartite_json) != nx.Graph:
        G = nx.Graph(nx.json_graph.node_link_graph(bipartite_json))
        # save the metadata inside graph
        G_new.graph = bipartite_json["metadata"]

        projection_set = bipartite_json["metadata"]["target_entity_type"] if bipartite_json["metadata"][
            "target_entity_type"] else projection_set
        secondary_set = bipartite_json["metadata"]["source_entity_type"] if bipartite_json["metadata"][
            "source_entity_type"] else secondary_set
    else:
        G = bipartite_json

    G_new.graph["graph_type"] = "unipartite"

    # primary node set : add nodes in the new graph with the same attributes
    nodes_set1 = [(n, d) for n, d in G.nodes(data=True) if d["entity_type"] == projection_set]
    G_new.add_nodes_from(nodes_set1)

    # secondary node set
    # nodes_set2 = [n for n, d in G.nodes(data=True) if d["entity_type"] == secondary_set]
    nodes_set2 = [n for n, d in G.nodes(data=True) if d["entity_type"] != projection_set]

    # for each contract, create a node between each possible pair of author
    for n in nodes_set2:
        #        if attribute_to_save != None:
        #            attribute_to_keep = G.node[n][attribute_to_save]
        neighbors_projection_set = [n2 for n2 in G[n] if G.nodes[n2]["entity_type"] == projection_set]
        node_pairs = list(itertools.combinations(neighbors_projection_set, 2))
        for pair in node_pairs:
            if is_time:
                # if we take time into account
                if pair in G_new.edges:
                    # if this time already exist for this edge, add one weight, if not initialize this time with weight one
                    if G.nodes[n][time_key] in G_new.edges[pair][weight_key]:
                        G_new.edges[pair][weight_key][G.nodes[n][time_key]] += 1
                    else:
                        G_new.edges[pair][weight_key][G.nodes[n][time_key]] = 1
                else:
                    # if edge does not exit, initialize it
                    G_new.add_edge(*pair, w={G.nodes[n][time_key]: 1})
            else:
                # Without time
                if pair in G_new.edges:
                    # if edge exist, add one weight
                    G_new.edges[pair][weight_key] += 1
                else:
                    # if edge does not exit, initialize it
                    weight = {weight_key: 1}
                    G_new.add_edge(*pair, **weight)

    if return_json:
        json_unipartite = nx.node_link_data(G_new)
        metadata = bipartite_json["metadata"]
        metadata["graph_type"] = "unipartite"
        json_unipartite["metadata"]: metadata
        del json_unipartite["multigraph"]
        del json_unipartite["directed"]
        return json_unipartite

    return G_new


if __name__ == '__main__':
    json = {"metadata": {"format": "2.0", "graph type": "bipartite", "nodes": "nodes", "links": "links",
                         "time_slot": "year", "entity_type": "entity_type", "source_entity_type": "contract",
                         "target_entity_type": "person", "prior_knowledge": {"Legrand": [0, 5, 1], "Dupont": [2, 6]}},
            "nodes": [{"id": 0, "entity_type": "person", "name": "Elise", "community": "Legrand",
                       "Prior Knowledge": "Legrand"},
                      {"id": 5, "entity_type": "person", "name": "John", "community": "Legrand",
                       "Prior Knowledge": "Legrand"},
                      {"id": 2, "entity_type": "person", "name": "Hubert", "community": "Dupont",
                       "Prior Knowledge": "Dupont"}, {"id": 7, "entity_type": "person", "name": "Antoine"},
                      {"id": 6, "entity_type": "person", "name": "Joseph", "community": "Dupont",
                       "Prior Knowledge": "Dupont"},
                      {"id": 1, "entity_type": "person", "name": "Jacques", "community": "Legrand",
                       "Prior Knowledge": "Legrand"},
                      {"id": 3, "entity_type": "person", "name": "Roze", "community": "Legrand"},
                      {"id": 4, "entity_type": "person", "name": "Vallet"},
                      {"id": 9, "entity_type": "person", "name": "Claude", "community": "Dupont"},
                      {"id": 10, "entity_type": "person", "name": "Guillaume"},
                      {"id": 11, "entity_type": "person", "name": "Madeleine", "community": "Dupont"},
                      {"id": 8, "entity_type": "person", "name": "Philippe", "community": "Dupont"},
                      {"id": 12, "entity_type": "person", "name": "Renexent"},
                      {"id": "_e_1", "ts": 1, "entity_type": "contract"},
                      {"id": "_e_0", "ts": 1, "entity_type": "contract"},
                      {"id": "_e_2", "ts": 1, "entity_type": "contract"},
                      {"id": "_e_3", "ts": 1, "entity_type": "contract"},
                      {"id": "_e_6", "ts": 1, "entity_type": "contract"},
                      {"id": "_e_5", "ts": 1, "entity_type": "contract"},
                      {"id": "_e_4", "ts": 1, "entity_type": "contract"},
                      {"id": "_e_3", "ts": 2, "entity_type": "contract"},
                      {"id": "_e_0", "ts": 2, "entity_type": "contract"},
                      {"id": "_e_1", "ts": 2, "entity_type": "contract"},
                      {"id": "_e_2", "ts": 2, "entity_type": "contract"},
                      {"id": "_e_4", "ts": 2, "entity_type": "contract"},
                      {"id": "_e_7", "ts": 2, "entity_type": "contract"},
                      {"id": "_e_5", "ts": 2, "entity_type": "contract"},
                      {"id": "_e_6", "ts": 2, "entity_type": "contract"},
                      {"id": "_e_1", "ts": 0, "entity_type": "contract"},
                      {"id": "_e_0", "ts": 0, "entity_type": "contract"},
                      {"id": "_e_3", "ts": 0, "entity_type": "contract"},
                      {"id": "_e_2", "ts": 0, "entity_type": "contract"},
                      {"id": "_e_1", "ts": 3, "entity_type": "contract"},
                      {"id": "_e_0", "ts": 3, "entity_type": "contract"},
                      {"id": "_e_2", "ts": 3, "entity_type": "contract"},
                      {"id": "_e_5", "ts": 3, "entity_type": "contract"},
                      {"id": "_e_4", "ts": 3, "entity_type": "contract"},
                      {"id": "_e_7", "ts": 3, "entity_type": "contract"},
                      {"id": "_e_3", "ts": 3, "entity_type": "contract"},
                      {"id": "_e_6", "ts": 3, "entity_type": "contract"}],
            "links": [{"source": "_e_1", "target": 0, "ts": 1}, {"source": "_e_1", "target": 2, "ts": 1},
                      {"source": "_e_0", "target": 0, "ts": 1}, {"source": "_e_0", "target": 1, "ts": 1},
                      {"source": "_e_2", "target": 0, "ts": 1}, {"source": "_e_2", "target": 3, "ts": 1},
                      {"source": "_e_3", "target": 0, "ts": 1}, {"source": "_e_3", "target": 4, "ts": 1},
                      {"source": "_e_6", "target": 5, "ts": 1}, {"source": "_e_6", "target": 7, "ts": 1},
                      {"source": "_e_5", "target": 5, "ts": 1}, {"source": "_e_5", "target": 6, "ts": 1},
                      {"source": "_e_4", "target": 4, "ts": 1}, {"source": "_e_4", "target": 5, "ts": 1},
                      {"source": "_e_3", "target": 0, "ts": 2}, {"source": "_e_3", "target": 5, "ts": 2},
                      {"source": "_e_3", "target": 6, "ts": 2}, {"source": "_e_0", "target": 0, "ts": 2},
                      {"source": "_e_0", "target": 1, "ts": 2}, {"source": "_e_1", "target": 0, "ts": 2},
                      {"source": "_e_1", "target": 3, "ts": 2}, {"source": "_e_2", "target": 0, "ts": 2},
                      {"source": "_e_2", "target": 2, "ts": 2}, {"source": "_e_2", "target": 3, "ts": 2},
                      {"source": "_e_2", "target": 4, "ts": 2}, {"source": "_e_2", "target": 5, "ts": 2},
                      {"source": "_e_4", "target": 0, "ts": 2}, {"source": "_e_4", "target": 5, "ts": 2},
                      {"source": "_e_4", "target": 7, "ts": 2}, {"source": "_e_4", "target": 8, "ts": 2},
                      {"source": "_e_4", "target": 9, "ts": 2}, {"source": "_e_7", "target": 9, "ts": 2},
                      {"source": "_e_7", "target": 10, "ts": 2}, {"source": "_e_5", "target": 10, "ts": 2},
                      {"source": "_e_5", "target": 11, "ts": 2}, {"source": "_e_6", "target": 10, "ts": 2},
                      {"source": "_e_6", "target": 11, "ts": 2}, {"source": "_e_6", "target": 12, "ts": 2},
                      {"source": "_e_1", "target": 0, "ts": 0}, {"source": "_e_1", "target": 2, "ts": 0},
                      {"source": "_e_0", "target": 0, "ts": 0}, {"source": "_e_0", "target": 1, "ts": 0},
                      {"source": "_e_3", "target": 5, "ts": 0}, {"source": "_e_3", "target": 7, "ts": 0},
                      {"source": "_e_2", "target": 5, "ts": 0}, {"source": "_e_2", "target": 6, "ts": 0},
                      {"source": "_e_1", "target": 0, "ts": 3}, {"source": "_e_1", "target": 10, "ts": 3},
                      {"source": "_e_0", "target": 0, "ts": 3}, {"source": "_e_0", "target": 11, "ts": 3},
                      {"source": "_e_0", "target": 9, "ts": 3}, {"source": "_e_2", "target": 0, "ts": 3},
                      {"source": "_e_2", "target": 9, "ts": 3}, {"source": "_e_2", "target": 10, "ts": 3},
                      {"source": "_e_2", "target": 12, "ts": 3}, {"source": "_e_5", "target": 5, "ts": 3},
                      {"source": "_e_5", "target": 7, "ts": 3}, {"source": "_e_4", "target": 5, "ts": 3},
                      {"source": "_e_4", "target": 6, "ts": 3}, {"source": "_e_7", "target": 2, "ts": 3},
                      {"source": "_e_7", "target": 3, "ts": 3}, {"source": "_e_7", "target": 4, "ts": 3},
                      {"source": "_e_7", "target": 5, "ts": 3}, {"source": "_e_7", "target": 7, "ts": 3},
                      {"source": "_e_7", "target": 8, "ts": 3}, {"source": "_e_7", "target": 9, "ts": 3},
                      {"source": "_e_3", "target": 2, "ts": 3}, {"source": "_e_3", "target": 3, "ts": 3},
                      {"source": "_e_6", "target": 9, "ts": 3}, {"source": "_e_6", "target": 10, "ts": 3}]}

    G = weighted_projection(json, projection_set='person', secondary_set='contract')
