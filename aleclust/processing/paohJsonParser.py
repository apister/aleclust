import os
import rapidjson
from networkx import json_graph
from jsonschema import validate, ValidationError

from aleclust.processing.parameters import Parameters
from aleclust.processing.projection import weighted_projection


class PaohJsonParser:
    def __init__(self, json, dynamic=False):
        self.json = json
        self.dynamic = dynamic

        schema_path = os.path.split(os.path.realpath(__file__))[0] + '/schema.json'
        with open(schema_path, "r") as f:
            self.schema = rapidjson.loads(f.read())

        try:
            self.validate_schema()
        except Exception as e:
            print(e)

        self.metadata = self.json["metadata"]
        self.parameters = Parameters(dynamic)
        self.parameters.parse_meta_data(self.metadata)

    def validate_schema(self):
        validate(schema=self.schema, instance=self.json)

    def own_json_to_graph(self, dynamic=False):
        if dynamic:
            graph = PaohJsonParser.json_to_dyn_graph(self.json, self.parameters.projection)
        else:
            graph = PaohJsonParser.json_to_graph(self.json, self.parameters.projection, self.parameters.projection_set)
        return graph

    @staticmethod
    def json_to_graph(json, projection, projection_set):
        if projection:
            graph = weighted_projection(json, projection_set=projection_set)
            graph.graph["bipartite"] = False
        else:
            graph = json_graph.node_link_graph(json, multigraph=False)
            graph.graph["bipartite"] = True

        # # Remove nodes from specific entity type, it is to transform multipartite graph into bipartite
        # if entity_type_to_remove is not None:
        #     graph = remove_entity_type(graph, entity_type=entity_type_to_remove)
        return graph

    @staticmethod
    def json_to_dyn_graph(json, projection):
        from dcd import projection, json_read

        dyn_graph = json_read.paoh_json_reader(json)
        dyn_graph.graph["bipartite"] = False
        if projection:
            dyn_graph = projection.projection(dyn_graph)
            dyn_graph.graph["bipartite"] = True

        return dyn_graph





