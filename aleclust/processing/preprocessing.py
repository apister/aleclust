# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 17:12:54 2019

@author: Aviz
"""

import networkx as nx
import json
import itertools
import rapidjson

from networkx import json_graph

import aleclust.utility as utility
import aleclust.processing.parameters as aleclust_parameters

# from graphconverter.io.json_converter import weighted_projection, convert_json
from aleclust.processing.projection import weighted_projection


def loadGraphfromJson(path=None, jsonData=None, nodeAttributes=["name"],
                      graphAttributes=["name", "ts_end", "ts_start", "ts_step"]):
    """Create static unweighted networkx graph from json file of paovis. Create an edge for each combination of nodes from the hyperedges. nodeAttribute are the attributes we want to keep for the nodes. Nodes and edges have an attribute 'times' corresponding to a list of times where they exist"""
    if jsonData is None:
        jsonData = json.loads(open(path).read())

    # Add graph attributes from graphAttributes which are in metadata of json
    graphAttrs = {}
    for metadataAttr in jsonData["metadata"]:
        if metadataAttr in graphAttributes:
            graphAttrs[metadataAttr] = jsonData["metadata"][metadataAttr]

    G = nx.Graph(**graphAttrs)
    # Keep only as node attributes the attributes of the json which match the nodeAttributes names
    for nodeInfo in jsonData["nodes"]:
        nodeAttr = {}
        # timesteps where the node exist
        times = []
        for attr in nodeInfo:
            if attr in nodeAttributes:
                nodeAttr[attr] = nodeInfo[attr]
            if attr == "data":
                for time in nodeInfo[attr]:
                    ts = time['ts']
                    times.append(ts)
        # add the times where the node exist (is a list) as attribute of the node
        nodeAttr['times'] = times
        G.add_node(nodeInfo["id"], **nodeAttr)

    # Adding edges
    # edgeInfo got all the hyperedges for on timestep
    for edgesInfo in jsonData["edges"]:
        time = edgesInfo['ts']
        # Save all the edges encountered at this time to avoid repetitions
        encounteredEdges = set()
        for edgeInfo in edgesInfo["list"]:
            # get all 2-sets combinations of the list of nodes of the hyperedges
            twoSets = list(itertools.combinations(edgeInfo["ids"], 2))
            # Add all 2-combination of the hyperedge as edge
            for twoset in twoSets:
                edge = (twoset[0], twoset[1])
                if edge in G.edges() and edge not in encounteredEdges:
                    G.edges[edge]["times"].append(time)
                else:
                    # Add the time as an attribute of the edge
                    G.add_edge(edge[0], edge[1], times=[time])
                encounteredEdges.add(edge)
    return G


# def loadWeightedGraphfromJson(path, nodeAttributes=["name"], graphAttributes = ["name", "ts_end", "ts_start", "ts_step"]):
#    """Create static weighted networkx graph from json file of paovis. Create an edge for each combination of nodes from the hyperedges. The weight of this edge is the number of time these 2 nodes appear together in different hyperedges (can be the same hyperedge but in different times). nodeAttribute are the attributes we want to keep for the nodes"""
#    jsonData = json.loads(open(path).read())
#    
#    # Add graph attributes from graphAttributes which are in metadata of json
#    graphAttrs = {}
#    for metadataAttr in jsonData["metadata"]:
#        if metadataAttr in graphAttributes:
#            graphAttrs[metadataAttr] = jsonData["metadata"][metadataAttr]
#            
#    G = nx.Graph(**graphAttrs)
#    # Keep only as node attributes the attributes of the json which match the nodeAttributes names
#    for nodeInfo in jsonData["nodes"]:
#        nodeAttr = {}
#        # Create a dictionnary of the attributes you want to keep in the graph
#        for attr in nodeInfo:
#            if attr in nodeAttributes:
#                nodeAttr[attr] = nodeInfo[attr]
#        # Add node
#        G.add_node(nodeInfo["id"], **nodeAttr)
#        
#    # Adding edges
#    # edgeInfo got all the hyperedges for on timestep
#    for edgesInfo in jsonData["edges"]:
#        #timeStep = edgesInfo['ts']
#        for edgeInfo in edgesInfo["list"]:
#            # get all 2-sets combinations of the list of nodes of the hyperedges
#            twoSets = list(itertools.combinations(edgeInfo["ids"], 2))
#            # Add all 2-combination of the hyperedge as edge
#            for edge in twoSets:
#                # if the 2 nodes appear in another hyperedge, increase the weight of 1
#                if edge in G.edges:
#                    G.edges[edge]["weight"] += 1
#                else:
#                    G.add_edge(edge[0], edge[1], weight=1)
#    return G


# %%

def graph_to_info(G, nodes_info=True, keep_only_ids=True, community_key="community", graph_info=True, edges_info=False):
    """
    From nx graph, return json data with all or some of the 3 components : {graph:{}, nodes:[], edges:[]}
    """
    Gjson = json_graph.node_link_data(G)
    json = {}
    if nodes_info:
        if keep_only_ids:
            for i, node_dict in enumerate(Gjson["nodes"]):
                Gjson["nodes"][i] = utility.delete_all_keys_from_dict(Gjson["nodes"][i],
                                                                      ["id", community_key, "communities"])
            json["nodes"] = Gjson["nodes"]
        else:
            json["nodes"] = Gjson["nodes"]
    if edges_info:
        json["edges"] = Gjson["edges"]
    if graph_info:
        json["graph"] = Gjson["graph"]
        # remove useless metadata in the graph
        for info in json["graph"].copy():
            if info in ["entity_type", "graph_type", "links", "nodes", "time", "time_format"]:
                del json["graph"][info]
    return json


def remove_entity_type(G, entity_type_key_name="entity_type", entity_type=None):
    """Remove all nodes of a precise entity_type. Can be used to remove nodes with a specific attribute value of
    other attributes. """

    # list of nodes of entity type to remove
    nodes = [n for n, attrs in G.nodes(data=True) if attrs[entity_type_key_name] == entity_type]
    # delete nodes
    G.remove_nodes_from(nodes)
    return G


# def json_to_graph(json, parameters=aleclust_parameters.Parameters(), entity_type_to_remove=None):
#     """
#     Convert json format to networkx graph.
#     """
#     if parameters.projection:
#         graph = weighted_projection(json, projection_set=parameters.projection_set)
#     else:
#         graph = json_graph.node_link_graph(json, multigraph=False)
#
#     # Remove nodes from specific entity type, it is to transform multipartite graph into bipartite
#     if entity_type_to_remove is not None:
#         graph = remove_entity_type(graph, entity_type=entity_type_to_remove)
#
#     print("json to graph conversion done")
#
#     return graph


def path_to_graph(path, projection=False, projection_set=None):
    """
    Load graph from path of json file representing a graph in aviz.json format or new json format. If new json format, can do a projection on a one-mode network.
    """
    from aleclust.processing.paohJsonParser import PaohJsonParser
    jsonData = rapidjson.loads(open(path, "r").read())
    G = PaohJsonParser.json_to_graph(jsonData, projection=projection, projection_set=projection_set)
    return G


def path_to_json(path):
    return rapidjson.loads(open(path, "r").read())


def remove_isolated_nodes(G):
    """
    Remove the nodes alone in the graph, keep their ids in a list in the metadata (G.graph[nodes_alone]) and return it
    """
    comps = list(nx.connected_components(G))
    nodes_alone = []

    for comp in comps:
        if len(comp) == 1:
            node_label = comp.pop()
            nodes_alone.append(node_label)

    G.graph['nodes_alone'] = nodes_alone

    for n in nodes_alone:
        G.remove_node(n)

    return nodes_alone


# %%
if __name__ == "__main__":
    schemaPath = "../data/new_json/schema.json"
    with open(schemaPath, "r") as f:
        schema = rapidjson.loads(f.read())

    avizDataPath = "../data/paohvis/aviz.json"
    avizJson = rapidjson.loads(open(avizDataPath, "r").read())

    Gaviz = json_to_graph(avizJson)
