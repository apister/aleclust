#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 10:35:16 2020

@author: alexis
"""
from collections.abc import Iterator

import aleclust.clustering.metric as metric
import aleclust.processing.preprocessing as preprocessing
import aleclust.clustering.clustering as clustering

from pyroaring import BitMap
from networkx import density


class Communities:
    def __init__(self, communities, graph, algorithm_name, nodes_without_community=None):
        # each community has an id
        if type(communities) == list:
            self.communities = communities
            self.communities_dict = {i + 1: com for i, com in enumerate(self.communities)}
        elif type(communities) == dict:
            self.communities = list(communities.values())
            self.communities_dict = communities
        elif type(communities) in (set, frozenset, tuple) or isinstance(communities, Iterator):
            self.communities = list(communities)
            self.communities_dict = {i + 1: com for i, com in enumerate(self.communities)}

        try:
            self.communities_bitmap_dict = {com_id: BitMap(community) for com_id, community in
                                        self.communities_dict.items()}
        except Exception as e:
            print("node ids are not numbers")

        self.algorithm_name = algorithm_name
        self.graph = graph
        self.N = len(self.communities)

        self.new_pk_names = {}

        # list of nodes without a community in the graph
        self.nodes_without_community = nodes_without_community

        # dict of statistics for each community, storing node and edge freq, density etc
        self.communities_stats = None
        self.compute_communities_stats()

        # dict of global metrics (modularity, coverage etc)
        self.metrics = None

    def compute_metrics(self, metrics, metrics_args=None):
        """
        Compute all the metrics given in arguments and store them in metrics dict
        """
        self.metrics = {}
        if metrics != None:
            for metric_name in metrics:
                if metrics_args == None:
                    metric_result = metric.compute_metric(self.graph, [comm_els for comm_els in self.communities],
                                                          metric_name)
                else:
                    metric_result = metric.compute_metric(self.graph, [comm_els for comm_els in self.communities],
                                                          metric_name, **metrics_args)

                self.metrics[metric_name] = metric_result

    def compute_communities_stats(self):
        """
        Compute local metrics for each community (need only the community in itself or the community subgraph to compute these)
        """
        self.communities_stats = {}
        for c_id, com in self.communities_dict.items():
            self.communities_stats[c_id] = {}
            # community subgraph
            com_subgraph = self.graph.subgraph(com)

            # Nodes frequencies
            n_freq = len(com)
            self.communities_stats[c_id]["node_frequency"] = n_freq

            # Edges frequencies
            e_freq = len(com_subgraph.edges)
            self.communities_stats[c_id]["edge_frequency"] = e_freq

            # density of community
            density_com = density(com_subgraph)
            self.communities_stats[c_id]["internal_density"] = density_com

    def to_json(self, with_metadata=True, metadata_key='graph'):
        '''
        Transform Node community into a dictionnary list of nodes ids with their community id for json output. This return the metatada stored in self.communities_stats and self.metrics in another key if with_metadata=True. metadata_key is by default called graph because previously this data was directly stored in the graph key of networkx graph objects.
        '''
        # nodes coms to dict
        json_output = {}
        nodes_json = []
        for c_id, com in self.communities_dict.items():
            for node_id in com:
                nodes_json.append({
                    'id': node_id,
                    'community': c_id
                })

        # add nodes without a community
        if self.nodes_without_community != None:
            for node_id in self.nodes_without_community:
                nodes_json.append({
                    'id': node_id,
                    'community': 'None'
                })

        json_output['nodes'] = nodes_json

        # metadata to dict
        if with_metadata:
            metadata = {}
            metadata['communities'] = self.communities_stats
            metadata['metrics'] = self.metrics
            metadata['n_communities'] = self.N

            json_output[metadata_key] = metadata

        return json_output

    def filter_nodes(self, nodes_to_filter):
        '''Remove occurences of any node in list nodes_to_filter in partition'''
        nodes_to_filter = BitMap(nodes_to_filter)
        communities = {i: com.difference(nodes_to_filter) for i, com in self.communities_bitmap_dict.items()}
        return communities

    def communities_dict_to_names(self, json):
        for pk_group, ids in self.communities_dict.items():
            self.new_pk_names[pk_group] = [self.id_to_name(json, id) for id in ids]
        return self.new_pk_names

    def id_to_name(self, json, id):
        for node in json["nodes"]:
            if node['id'] == id:
                return node["name"]
        raise Exception("id not found in graph data")

    def label_vector_transform(self):
        pass


# %% Test
if __name__ == '__main__':
    g = preprocessing.path_to_graph("../data/new_json/test.json")
    coms = clustering.applyClusteringAlgorithm(g, 'louvain')

    node_clustering = Communities(coms, g)
    node_clustering.compute_metrics(['coverage', 'modularity'])

    json = node_clustering.to_json()
    print(json)
