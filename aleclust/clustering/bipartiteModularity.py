# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 16:09:42 2019

@author: Aviz
"""
import itertools
import math
import random

import rapidjson

import networkx as nx

import aleclust.processing.preprocessing as preprocessing
import aleclust.utility as utility
import aleclust.clustering.clustering as clustering


def compute_n_shared_teams(G, ni, nj):
    """
        Compute number of shared team beatween ni and nj
    """
    ni_neighbors = G[ni]
    nj_neighbors = G[nj]
    shared_neighbors = set(ni_neighbors).union(set(nj_neighbors))
    return len(shared_neighbors)


def compute_term2_term4(G, entity_type_key="entity_type", team_name="publication"):
    """
    Compute 2nd and 4th term of formula : sum of (degree) * (degree - 1) and
    (sum(degree))^2 of all team nodes
    """
    team_nodes = utility.get_nodes_by_attribute(G, entity_type_key, team_name)
    term2 = 0
    term4 = 0
    for n in team_nodes:
        n_neighbors = len(G[n])
        term2 += n_neighbors * (n_neighbors - 1)
        term4 += n_neighbors
    term4 = term4 ** 2
    return term2, term4


def bipartite_modularity(G, community_key="community", set_type_key="entity_type",
                         bimodularity_entity_type="people", verbose=False):
    """
        Compute bipartite modularity, introduced by [Guimera, 2007]
    """
    # list of community ids
    community_ids = utility.get_attribute_range(G, attribute_key=community_key)

    # nodes of type people
    people_nodes = utility.get_nodes_by_attribute(G, set_type_key, bimodularity_entity_type)

    modularity = 0

    term2, term4 = compute_term2_term4(G)

    # TODO : if term2 = 0, there is a division by 0 returning an error.
    # I don't know how to handle this. For now, return 0 as the modularity
    if term2 == 0:
        return 0

    for c_id in community_ids:
        # nodes of people type and of community c_id
        nodes_ci = [n for n in people_nodes if G.nodes[n][community_key] == c_id]
        nodes_pairs = list(itertools.combinations(nodes_ci, 2))

        term1 = 0
        term3 = 0

        for pair in nodes_pairs:
            # neighbors of ni and nj
            ni_neighbors = G[pair[0]]
            nj_neighbors = G[pair[1]]

            # We multiply by 2 because we want the pair (i,j) and (j,i)
            term1 += len(set(ni_neighbors).intersection(set(nj_neighbors))) * 2
            term3 += len(ni_neighbors) * len(nj_neighbors) * 2

        if verbose:
            print("\ncommunity ", c_id, "\nterm 1 :", term1, "\nterm 2 : ", term2,
                  "\nterm 3 : ", term3, "\nterm 4 : ", term4)
        term_c = (term1 / term2) - (term3 / term4)

        modularity += term_c
    return modularity


def bipartite_modularity_handler(G, communities, bimodularity_entity_type, **kwargs):
    """
    This function have the same format of other metric functions f(G, communities)
    """
    G = clustering.clustersIntoAttributes(G, communities, cluster_key_name="community",
                                          overlapping=False)
    bimodularity = bipartite_modularity(G, set_type_key=bimodularity_entity_type, **kwargs)
    return bimodularity


def bipartite_modularity_cost(G):
    return -bipartite_modularity(G)


def change_node_community_random(G, node_list, community_ids, community_key="community"):
    """
        Randomly change the community of a random node
    """
    node_id = random.sample(node_list, k=1)[0]
    c_node = G.nodes[node_id][community_key]

    new_c = c_node
    while new_c == c_node:
        new_c = random.sample(community_ids, k=1)[0]
    G.nodes[node_id][community_key] = new_c

    return node_id, c_node


# select only on mode
def merge_communities(G, community_ids, community_key="community"):
    """
        merge 2 randomly selected communities in G : all the nodes of c2 get assigned to c1
    """
    c1, c2 = random.sample(community_ids, k=2)
    # nodes_c2 = utility.get_nodes_by_attribute(G, community_key, c2)
    nodes_c2 = [n for n, a in G.nodes.data()
                if a["entity_type"] == "people" and a[community_key] == c2]
    nodes_c2_dict = {n: {community_key: c1} for n in nodes_c2}
    nx.set_node_attributes(G, nodes_c2_dict)
    community_ids.remove(c2)
    return c2, nodes_c2


def split_community(G, nodes_c, c_id, community_ids, community_key="community"):
    """
        Split a community inside G into 2 separate communities
    """
    # c_id = G.node[nodes_c[0]][community_key]

    max_id = max(community_ids)
    c_id_new = max_id + 1
    while c_id_new in community_ids:
        c_id_new += 1

    community_ids.add(c_id_new)

    for n in nodes_c:
        r = random.random()
        if r < 0.5:
            G.nodes[n][community_key] = c_id
        else:
            G.nodes[n][community_key] = c_id_new

    return c_id_new


def is_keeping_changes(cost0, cost1, T):
    """
        Compare cost at time t and t+1 and compute the probability of keeping changes
    """
    if cost1 < cost0:
        keep_changes = True
    else:
        p = math.exp(-(cost1 - cost0) / T)
        rand = random.random()
        keep_changes = rand < p
    return keep_changes


def simulated_annealing(G, cost_function=bipartite_modularity_cost, Tinit=1, Tmin=-1,
                        itmax=2, c=0.995, f=0.01,
                        local_changes=True, global_changes=True, recursive=False,
                        set_type_key="entity_type", target_entity_type="people",
                        community_key="community"):
    """
    Simulated annealing for the optimizaition of the bipartite modularity.
    """
    random.seed(0)
    if not recursive:
        # Initialize random communities
        utility.initialize_random_communities(G, k=3, entity_type_key=set_type_key,
                                              entity_type=target_entity_type)

    community_ids = utility.get_attribute_range(G, attribute_key=community_key)

    people_nodes = utility.get_nodes_by_attribute(G, set_type_key, target_entity_type)
    N_nodes = len(people_nodes)

    #    if recursive == False:
    #        print("community ids : ", community_ids)
    #        print(type(f), N_nodes)
    #        print( int(f * (N_nodes * N_nodes)))
    #        print("number of local changes at each Temperature level : ", int(f * (N_nodes * N_nodes)))
    #        print("number of global changes at each Temperature level : ", int(f * N_nodes))

    cost0 = cost_function(G)
    T = Tinit

    it = 1
    while (it <= itmax) and (T > Tmin):  # T loop
        if not recursive:
            print(it, T, cost0, community_ids, utility.get_attribute_distribution(G, "community"))

        if local_changes:
            for _ in range(int(f * (N_nodes ** 2))):
                # local changes for one T

                # randomly change the community of one node
                node_id, old_c = change_node_community_random(G, people_nodes,
                                                              community_ids,
                                                              community_key=community_key)

                # new cost
                cost1 = cost_function(G)

                keep_changes = is_keeping_changes(cost0, cost1, T)
                if keep_changes:
                    # keep change and new associated cost
                    cost0 = cost1
                else:
                    # change graph to previous state
                    G.nodes[node_id][community_key] = old_c

        if global_changes:
            for _ in range(int(f * N_nodes)):
                # global changes

                r = random.random()

                if len(community_ids) > 2 and r < 0.5:
                    # merging of 2 communities
                    c2, old_nodes_c2 = merge_communities(G, community_ids,
                                                         community_key=community_key)
                    cost1 = cost_function(G)
                    keep_changes = is_keeping_changes(cost0, cost1, T)
                    if keep_changes:
                        cost0 = cost1
                    else:
                        # resplit the community : rechange the community ids of all c2 nodes
                        # with c2 id and put back the c2 id on community id list
                        nodes_c2_dict = {n: {community_key: c2} for n in old_nodes_c2}
                        nx.set_node_attributes(G, nodes_c2_dict)
                        community_ids.add(c2)

                else:
                    # splitting : use a simulated annealing
                    #                    print("START OF SPLITTING")

                    # sort communities by their frequencies : select only a big community
                    # for splitting
                    community_dist = utility.get_attribute_distribution(G, community_key)
                    del community_dist[None]
                    sorted_communities = sorted(community_dist, key=community_dist.get)
                    big_communities = sorted_communities[int(len(sorted_communities) / 2):]

                    c_id = random.sample(big_communities, k=1)[0]
                    nodes_c = {n for n, a in G.nodes.data()
                               if a[set_type_key] == target_entity_type and a[community_key] == c_id}

                    # add all "teams" of nodes_c into the module subgraph
                    neighbors_nodes_c = set()
                    for n in nodes_c:
                        neighbors = set(G[n])
                        for neighbor in neighbors:
                            neighbors_nodes_c.add(neighbor)
                    module_nodes = nodes_c.union(neighbors_nodes_c)

                    # subgraph representing the community splitting. When you modify the
                    # attributes of the subgraph, they change in the original graph as well
                    module = G.subgraph(module_nodes)

                    c_id_new = split_community(G, nodes_c, c_id, community_ids)

                    simulated_annealing(module, Tinit=Tinit, Tmin=T, global_changes=False,
                                        recursive=True)

                    cost1 = cost_function(G)
                    keep_changes = is_keeping_changes(cost0, cost1, T)
                    if keep_changes:
                        cost0 = cost1
                    else:
                        # remerge the community
                        for n in nodes_c:
                            G.nodes[n][community_key] = c_id
                        community_ids.remove(c_id_new)

        #                    print("END OF SPLITTING")
        T = T * c
        it += 1

    if not recursive:
        # return communities as dictionnary
        return clustering.exportCommunity(G, entity_type_key=set_type_key,
                                          entity_type=target_entity_type)


def gen_bipartite_com(n_team, actors_range_a, actors_range_b):
    """
    Generate bipartite graph with communities perfectly separated by teams
    """
    import random as rn
    g = nx.Graph()

    id_act = -1
    for x in range(n_team):
        g.add_node(x, entity_type="publication")
        n_actor = rn.randint(actors_range_a, actors_range_b)
        for y in range(n_actor):
            g.add_node(id_act, entity_type="people", community=x)
            g.add_edge(id_act, x)
            id_act -= 1

    return g


# %%

if __name__ == "__main__":
    # G = preprocessing.loadGraphFromPath("../data/new_json/graph.json", entity_type_to_remove="keyword")
    infoVisDataPath = "../data/new_json/IEEE_VIS_2012_2016_InfoVis.json"
    json_data = rapidjson.loads(open(infoVisDataPath).read())
    G = preprocessing.json_to_graph(json_data, projection=False)

    # %%
    utility.initialize_random_communities(G, k=3,
                                          entity_type_key="entity_type", entity_type="people")
    G = clustering.doClustering(G, clustering_algo="Guimera", f=0.0001, itmax=1)
    # modul = bipartite_modularity(G)

    # %%
    coms = simulated_annealing(G, itmax=1, f=0.1)

    # %%

    eurovis_path = "../data/new_json/EuroVis_Data_2004_2006.json"
    G2 = preprocessing.path_to_graph(eurovis_path, projection=False)
    utility.initialize_random_communities(G2, k=3, entity_type_key="entity_type",
                                          entity_type="people")

    # %%
    simulated_annealing(G2, cost_function=bipartite_modularity_cost, c=0.1, f=0.01,
                        local_changes=True, global_changes=True, itmax=2)

    # %%
    g = gen_bipartite_com(160, 4, 30)
