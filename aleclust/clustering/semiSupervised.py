#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 13:35:20 2020

@author: alexis
"""

# Code found in stackoverflow : https://datascience.stackexchange.com/questions/45459/how-to-use-scikit-learn-label-propagation-on-graph-structured-data
# https://hub.gke.mybinder.org/user/thibaudmartinez-bel-propagation-rt7av7vz/notebooks/notebook.ipynb

from abc import abstractmethod
from collections import defaultdict
import networkx as nx
import numpy as np
import pandas as pd
import torch

import aleclust.processing.preprocessing as preprocessing

class BaseLabelPropagation:
    """Base class for label propagation models.

    Parameters
    ----------
    adj_matrix: torch.FloatTensor
        Adjacency matrix of the graph.
    """
    def __init__(self, adj_matrix):
        self.norm_adj_matrix = self._normalize(adj_matrix)
        self.n_nodes = adj_matrix.size(0)
        self.one_hot_labels = None
        self.n_classes = None
        self.labeled_mask = None
        self.predictions = None

    @staticmethod
    @abstractmethod
    def _normalize(adj_matrix):
        raise NotImplementedError("_normalize must be implemented")

    @abstractmethod
    def _propagate(self):
        raise NotImplementedError("_propagate must be implemented")

    def _one_hot_encode(self, labels):
        # Get the number of classes
        classes = torch.unique(labels)
        classes = classes[classes != -1]
        self.n_classes = classes.size(0)

        # One-hot encode labeled data instances and zero rows corresponding to unlabeled instances
        unlabeled_mask = (labels == -1)
        labels = labels.clone()  # defensive copying
        labels[unlabeled_mask] = 0
        self.one_hot_labels = torch.zeros((self.n_nodes, self.n_classes), dtype=torch.float)
        self.one_hot_labels = self.one_hot_labels.scatter(1, labels.unsqueeze(1), 1)
        self.one_hot_labels[unlabeled_mask, 0] = 0

        self.labeled_mask = ~unlabeled_mask

    def fit(self, labels, max_iter, tol):
        """Fits a semi-supervised learning label propagation model.

        labels: torch.LongTensor
            Tensor of size n_nodes indicating the class number of each node.
            Unlabeled nodes are denoted with -1.
        max_iter: int
            Maximum number of iterations allowed.
        tol: float
            Convergence tolerance: threshold to consider the system at steady state.
        """
        self._one_hot_encode(labels)

        self.predictions = self.one_hot_labels.clone()
        prev_predictions = torch.zeros((self.n_nodes, self.n_classes), dtype=torch.float)

        for i in range(max_iter):
            # Stop iterations if the system is considered at a steady state
            variation = torch.abs(self.predictions - prev_predictions).sum().item()

            if variation < tol:
                print(f"The method stopped after {i} iterations, variation={variation:.4f}.")
                break

            prev_predictions = self.predictions
            self._propagate()

    def predict(self):
        return self.predictions

    def predict_classes(self):
        return self.predictions.max(dim=1).indices


class LabelPropagation(BaseLabelPropagation):
#    def __init__(self, adj_matrix):
#        super(LabelPropagation, self).__init__(adj_matrix)

    @staticmethod
    def _normalize(adj_matrix):
        """Computes D^-1 * W"""
        degs = adj_matrix.sum(dim=1)
        degs[degs == 0] = 1  # avoid division by 0 error
        return adj_matrix / degs[:, None]

    def _propagate(self):
        self.predictions = torch.matmul(self.norm_adj_matrix, self.predictions)

        # Put back already known labels
        self.predictions[self.labeled_mask] = self.one_hot_labels[self.labeled_mask]

    def fit(self, labels, max_iter=1000, tol=1e-3):
        super().fit(labels, max_iter, tol)


class LabelSpreading(BaseLabelPropagation):
    def __init__(self, adj_matrix):
        super().__init__(adj_matrix)
        self.alpha = None

    @staticmethod
    def _normalize(adj_matrix):
        """Computes D^-1/2 * W * D^-1/2"""
        degs = adj_matrix.sum(dim=1)
        norm = torch.pow(degs, -0.5)
        norm[torch.isinf(norm)] = 1
        return adj_matrix * norm[:, None] * norm[None, :]

    def _propagate(self):
        self.predictions = (
            self.alpha * torch.matmul(self.norm_adj_matrix, self.predictions)
            + (1 - self.alpha) * self.one_hot_labels
        )

    def fit(self, labels, max_iter=1000, tol=1e-3, alpha=0.5):
        """
        Parameters
        ----------
        alpha: float
            Clamping factor.
        """
        self.alpha = alpha
        super().fit(labels, max_iter, tol)



def labelPropagation_handler(G, prior_knowledge, algorithm='label-propagation',
                             alpha=0.5, max_iter=1000, tol=1e-3):
    # Map node labels to contiguous integers
    label_for_node = dict((i, v) for i, v in enumerate(G.nodes()))
    node_for_label = dict((label_for_node[i], i) for i in range(len(G)))

    # Map PK groups labels to contiguous integers
    prior_for_pkid = {pk_name: i for i, pk_name in enumerate(prior_knowledge)}
    pkid_for_prior = {pkid: pk_name for pk_name, pkid in prior_for_pkid.items()}

    adj_matrix = nx.adjacency_matrix(G).toarray()

    labels = np.full(len(G), -1.)

    # fill prior knowledge in the labels vector
    for pk_id, pk in prior_knowledge.items():
        for node in pk:
            labels[node_for_label[node]] = prior_for_pkid[pk_id]

    # Create input tensors
    adj_matrix_t = torch.FloatTensor(adj_matrix)
    labels_t = torch.LongTensor(labels)

    if algorithm == 'label-propagation':
        label_propagation = LabelPropagation(adj_matrix_t)
        label_propagation.fit(labels_t, max_iter, tol)
    elif algorithm == 'label-spreading':
        label_propagation = LabelSpreading(adj_matrix_t)
        label_propagation.fit(labels_t, max_iter, tol, alpha)

    # label for each node
    output_labels = label_propagation.predict_classes()

    # transform label vector to community dict
    communities = defaultdict(list)
    for i, n in enumerate(output_labels.numpy()):
        communities[pkid_for_prior[n]].append(label_for_node[i])

    return dict(communities)



#%%
if __name__ == '__main__':
    import pandas as pd
    import numpy as np
    import networkx as nx

    G = preprocessing.path_to_graph("../data/new_json/v2/test_prior_knowledge.json",
                                    projection=True)
    coms = labelPropagation_handler(G, G.graph['prior_knowledge'])

#    # Create caveman graph
#    n_cliques = 4
#    size_cliques = 10
#    caveman_graph = nx.connected_caveman_graph(n_cliques, size_cliques)
#    adj_matrix = nx.adjacency_matrix(caveman_graph).toarray()
#
#    # Create labels
#    labels = np.full(n_cliques * size_cliques, -1.)
#
#    # Only one node per clique is labeled. Each clique belongs to a different class.
#    labels[0] = 0
#    labels[size_cliques] = 1
#    labels[size_cliques * 2] = 2
#    labels[size_cliques * 3] = 3
#
#    # Create input tensors
#    adj_matrix_t = torch.FloatTensor(adj_matrix)
#    labels_t = torch.LongTensor(labels)
#
#    # Learn with Label Propagation
#    label_propagation = LabelPropagation(adj_matrix_t)
#    print("Label Propagation: ", end="")
#    label_propagation.fit(labels_t)
#    label_propagation_output_labels = label_propagation.predict_classes()
#
#    # Learn with Label Spreading
#    label_spreading = LabelSpreading(adj_matrix_t)
#    print("Label Spreading: ", end="")
#    label_spreading.fit(labels_t, alpha=0.8)
#    label_spreading_output_labels = label_spreading.predict_classes()
