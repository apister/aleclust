#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 11 10:46:08 2020

@author: alexis
"""
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from collections import defaultdict

from aleclust.embedding.slepian import slepian_embedding

def spectral_clustering(G, embedding='slepian', clustering_alg='kmeans'):
    print()
    A = nx.adjacency_matrix(G).todense()
    A = np.array(A)
    embeddings =  slepian_embedding(A)
    
    if clustering_alg == 'kmeans':
        kmeans = KMeans(n_clusters=3, random_state=0).fit(embeddings)
    else:
        print('clustering algorithm unknown')
        
    print(kmeans.labels_)
    
    coms = defaultdict(list)
    order = list(G.nodes)
    for i, com in enumerate(kmeans.labels_):
        node_id = order[i]
        coms[int(com)].append(node_id)
        
    print('COMS: ', coms)
        
    return dict(coms)
        
    
#    plt.plot(embeddings[:, 0], embeddings[:,1], 'ro')
    
    
    
if __name__ == '__main__':
    G = nx.random_graphs.erdos_renyi_graph(40, 0.3)
    
    spectral_clustering(G)
    