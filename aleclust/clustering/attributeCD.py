#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 17:20:36 2020

@author: alexis
"""
import copy

import networkx as nx

def timeToAttribute(G, time_dict_key="w", filter_f=None):
    # compute a time attribute for each node from the time in the edge. For now take the minimum possible
    for u in G.nodes:
        # one list of dict containing as key the timeslots and value the number of contracts for each node
        times_contracts_u = [G.get_edge_data(*e)[time_dict_key] for e in list(G.edges(u))]
        
        # time attribute for each node. For now, take the minimum time
        u_time = min([min(edge_times.keys()) for edge_times in times_contracts_u])

        # G.node[u]['time'] = int(u_time)
        G.nodes[u]['time'] = int(u_time)


ATTRIBUTES_TO_FILTER = ["value"]
def attribute_clustering_preprocessing(G, clusteringAlgo, **kwargs):
    """process the parameters for attributes based graph clustering"""
    # name of attributes to use
    print(kwargs)
    attributes = kwargs['attributes']

    for attribute_to_filter in ATTRIBUTES_TO_FILTER:
        if attribute_to_filter in attributes:
            attributes.remove(attribute_to_filter)

    print("a", attributes)

    # add time as an attribute in the graph
    if 'time' in attributes:
        timeToAttribute(G)

    all_attributes = dict(G.nodes.data())
    labels = copy.deepcopy(all_attributes)

    for n_id, attributes_dict in all_attributes.items():
        for att in attributes_dict:
            if att not in attributes:
                del labels[n_id][att]

        # If attribute not specified, put "misc", otherwise eva wont work
        for att in attributes:
            if att not in labels[n_id].keys():
                if clusteringAlgo == "eva":
                    labels[n_id][att] = "misc"
                elif clusteringAlgo == "ilouvain":
                    labels[n_id][att] = 0


    kwargs['labels'] = labels
    del kwargs['attributes']

    # Not necessarily with new version
    # ids = {i: i for i in G.nodes}
    # if clusteringAlgo == 'ilouvain':
    #     kwargs['id'] = ids

    return kwargs