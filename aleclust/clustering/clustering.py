# -*- coding: utf-8 -*-
"""
Created on Thu Oct 10 12:42:15 2019

@author: Aviz
"""
import networkx as nx
import numpy as np
from cdlib import NodeClustering, AttrNodeClustering

from networkx.algorithms import community
from networkx import density

import aleclust.globals as glob
import aleclust.processing.preprocessing as preprocessing
import aleclust.utility as utility
import aleclust.clustering.communities as nodeClustering

# TODO : Move to metric
from aleclust.clustering.attributeCD import attribute_clustering_preprocessing

METRICS = {
    "coverage": community.quality.coverage,
    "performance": community.quality.performance,
    "modularity": community.quality.modularity,
    # "bimodularity": bipartiteModularity.bipartite_modularity_handler
}

METRICS_HIERARCHICAL_DESCRIPTION = {
    "unipartite": {
        "partition": {
            "coverage",
            "performance",
            "modularity"
        }
    },
    "bipartite": {
        "partition": {
            "bimodularity"
        },
        "bipartition": {
            "bimodularity"
        }
    }
}


def clustersIntoAttributes(G, clusters, cluster_key_name="community", overlapping=False):
    """Tag each node inside networkx graph with its community id, from either a tuple of commmunities ({12,1,13}, {2,3,4}, ...) or a dict {1: {1,2,3}, 2: {8,9,23}, ...}. In the first case, the community ids are the indexes in the tuple. If overlapping is True, the community key is a list of the communities of the nodes"""
    if type(clusters) != dict:
        # if clusters is not a dict, convert it into one
        clusters = {i: comm for i, comm in enumerate(clusters)}

    if overlapping:
        # utility.add_list_to_all_nodes(G, attribute_name=cluster_key_name)
        for n in G.nodes():
            G.nodes[n][cluster_key_name] = []
        for com_id, com in clusters.items():
            for node in com:
                G.nodes[node][cluster_key_name].append(com_id)
        # transform list of len = 1 into their element
        for n in G.nodes():
            if type(G.nodes[n][cluster_key_name]) == list and len(G.nodes[n][cluster_key_name]) == 1:
                G.nodes[n][cluster_key_name] = G.nodes[n][cluster_key_name][0]
    else:
        for n in G.nodes():
            G.nodes[n][cluster_key_name] = None
        for com_id, com in clusters.items():
            for node in com:
                G.nodes[node][cluster_key_name] = com_id

    # if a node does not have any community, set the community value as "None"
    for n in G.nodes():
        if G.nodes[n][cluster_key_name] in [[], ""] or cluster_key_name not in G.nodes[n]:
            G.nodes[n][cluster_key_name] = "None"

    return G


def exportCommunity(G, community_key="community", entity_type_key="entity_type", entity_type=None):
    """
    Export the communities ids of nodes inside a graph into a dict of the form {community_id : list_of_nodes_id}. if entity_type is not None, do it only for the nodes of the specific entity type
    """
    communities = {}
    if entity_type != None:
        nodes = utility.get_nodes_by_attribute(G, entity_type_key, entity_type, data=True)  # dict
    else:
        nodes = dict(G.nodes(data=community_key))
    for n_id in nodes:
        c_id = nodes[n_id]["community"]
        if c_id in communities:
            communities[c_id].add(n_id)
        else:
            communities[c_id] = {n_id}
    return communities


# useless finally for now
def remove_other_attributes(G, attributes_to_keep):
    G_cleaned = G.copy()
    for n, att_dict in G.nodes.data():
        for att in att_dict:
            if att not in attributes_to_keep:
                del G_cleaned.node[n][att]

    return G_cleaned


# TODO : remove alones nodes in the beginning and put them back later with no communities
def applyClusteringAlgorithm(G, clustering_algo, isolated_nodes=None, **kwargs):
    """
    apply clustering algorithm from its name and return partition
    """
    if clustering_algo == "fluid" or clustering_algo == "spectral-co-clustering":
        # for fluid algorithm, apply it on the biggest connected component if the others have length one
        # for SCC, remove nodes alones
        comps = list(nx.connected_components(G))

        if len(comps) > 1:
            comp_lengths = [len(c) for c in comps]
            max_len = max(comp_lengths)
            max_ind = np.where(max_len == comp_lengths)
            comp_lengths.remove(max_len)

            if set(comp_lengths) == set([1]):
                # removes nodes alones
                G = G.subgraph(max(comps, key=len)).copy()
            else:
                if clustering_algo == "fluid":
                    return "The fluid community detection algorithm requires a connected graph."

    # print("K ", kwargs)
    if clustering_algo in glob.ATTRIBUTE_UNIPARTITE_CDA:
        kwargs = attribute_clustering_preprocessing(G, clustering_algo, **kwargs)

    # print(clustering_algo, kwargs, G.nodes.data())
    clusters = glob.STATIC_CDA[clustering_algo]["function"](G, **kwargs)

    # Hierarchical result    
    if clustering_algo == "girvan-newman":
        for part in clusters:
            partition = part
            break
        clusters = partition

    # if the CD function is from cdlib, take only the communities attribute
    if type(clusters) in [NodeClustering, AttrNodeClustering]:
        clusters = clusters.communities

    # print(list(clusters))
    clusters = nodeClustering.Communities(clusters, G, clustering_algo, isolated_nodes)

    return clusters


def compute_communities_frequencies(G, communities, is_dict=False):
    if type(communities) != dict:
        # Transformation in dict. We use the index of community set as com id
        communities = {i: com for i, com in enumerate(communities)}

    communities_info = {i: {} for i in communities}

    for c_id, com in communities.items():
        # community subgraph
        com_subgraph = G.subgraph(com)

        # Nodes frequencies
        n_freq = len(com)
        communities_info[c_id]["node_frequency"] = n_freq

        # Edges frequencies
        e_freq = len(com_subgraph.edges)
        communities_info[c_id]["edge_frequency"] = e_freq

        # density of community
        density_com = density(com_subgraph)
        communities_info[c_id]["internal_density"] = density_com

    return communities_info


def add_isolated_nodes(G):
    for isolated_n in G.graph['nodes_alone']:
        G.add_node(isolated_n, community='None')


def post_processing_clustering(G, clustering_result, community_type,
                               metrics_to_compute=["coverage", "performance", "modularity"], metrics_args=None):
    """
    From a Graph and a community structure, merge the two, compute metrics, add infos to metadata
    """
    #    n_coms = len(clustering_result.values())
    #    G.graph["n_communities"] = n_coms
    #
    #    communities_info = compute_communities_frequencies(G, clustering_result)
    #    G.graph["communities"] = communities_info
    #
    #    if metrics_to_compute != None and metrics_to_compute != "None":
    #        # Global metrics can only be computed for partitions (all nodes must have a community)
    #        G.graph["metrics"] = {}
    #
    #        print("compute metrics")
    #        for quality_function_key in metrics_to_compute:
    #            # the nx quality functions needs the community in the form of a list of sets
    #
    #            if metrics_args == None:
    #                quality_measure = metric.compute_metric(G, [comm_els for comm_els in clustering_result.values()], quality_function_key)
    #            else:
    #                quality_measure = metric.compute_metric(G, [comm_els for comm_els in clustering_result.values()], quality_function_key, **metrics_args)
    #
    #            #quality_measure = METRICS[quality_function_key](G, [comm_els for comm_els in clustering_result.values()])
    #            G.graph["metrics"][quality_function_key] = quality_measure
    #
    #
    #    print("merging graph and cluster results")
    #    if community_type in ["overlapping", "cover"]:
    #        G_clustered = clustersIntoAttributes(G, clustering_result, overlapping=True)
    #    else:
    #        G_clustered = clustersIntoAttributes(G, clustering_result, overlapping=False)

    # add previously deleted isolated nodes and put 'None' as community id
    if 'nodes_alone' in G.graph:
        add_isolated_nodes(G)

    return G_clustered


def doClustering(G, clustering_algo, metrics_to_compute=["coverage", "performance", "modularity"], metrics_args=None,
                 **kwargs):
    isolated_nodes = preprocessing.remove_isolated_nodes(G)
    node_clustering = applyClusteringAlgorithm(G, clustering_algo, **kwargs)

    # node_clustering = nodeClustering.NodeClustering(clustering_result, G, clustering_algo, isolated_nodes)
    node_clustering.compute_metrics(metrics_to_compute, metrics_args)

    if 'nodes_alone' in G.graph:
        add_isolated_nodes(G)

    # dict containing nodes community ids and metrics in 'graph' key
    result_json = node_clustering.to_json()

    return result_json