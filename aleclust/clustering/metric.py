# -*- coding: utf-8 -*-
"""
Created on Fri Jan 17 13:50:29 2020

@author: Aviz
"""
import aleclust.clustering.clustering as clustering

def compute_metric(G, clusters, metric_name, **kwargs):
    assert metric_name in clustering.METRICS.keys()
    
    if metric_name == "bimodularity":
        metric = clustering.METRICS[metric_name](G, clusters, kwargs["bimodularity_entity_type"])
    else:
        metric = clustering.METRICS[metric_name](G, clusters)
    return metric    
