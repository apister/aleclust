#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 20 14:49:57 2019

@author: alexis
"""
from collections import defaultdict

from networkx import bipartite

from sklearn.cluster import SpectralCoclustering

import aleclust.processing.preprocessing as preprocessing
import aleclust.utility as utility
import aleclust.clustering.clustering as clustering


def spectral_co_clustering(G, k=5, seed=0, entity_type_key="entity_type",
                           source_entity_type="document", keep_set1=False):
    """ Give a community label for the second set of nodes if keep_set2 = true"""
    # select nodes of type 1 in bipartite graph
    nodes_set1 = utility.get_nodes_by_attribute(G, entity_type_key, source_entity_type)
    nodes_set2 = set(G) - set(nodes_set1)

    # bipartite graph
    bimat = bipartite.biadjacency_matrix(G, row_order=nodes_set1, column_order=nodes_set2)
    # Initialize sklearn model
    model = SpectralCoclustering(n_clusters=k, random_state=seed)
    model.fit(bimat)

    # labels of biclusters for each row and column
    rows_labels = model.row_labels_
    cols_labels = model.column_labels_

    # fill the dict of communities from the labels of each row and column
    # convert the community ids from numpy int to int
    communities = defaultdict(list)

    if keep_set1:
        for i, node in enumerate(nodes_set1):
            communities[int(rows_labels[i]) + 1].append(node)

    for i, node in enumerate(nodes_set2):
        communities[int(cols_labels[i]) + 1].append(node)

    return dict(communities)


if __name__ == "__main__":
    eurovis_path = "../data/new_json/EuroVis_Data_2004_2006.json"
    G = preprocessing.path_to_graph(eurovis_path, projection=False)
    coms = spectral_co_clustering(G, k=3, source_entity_type='publication')
#    G = clustering.doClustering(G, "spectralCoClustering", k=5, seed=0)
    #%%
    Gbi = preprocessing.path_to_graph("../data/new_json/v2/test_MB_names.json",
                                      projection=False)
    Gbi = clustering.doClustering(Gbi, "spectral-co-clustering", k=5, seed=0,
                                  source_entity_type="document", metrics_to_compute=None)

    #%%
    Gbi = preprocessing.path_to_graph("../data/new_json/v2/MB_demo.json", projection=False)
    coms = clustering.applyClusteringAlgorithm(Gbi, 'spectral-co-clustering')
    results = clustering.doClustering(Gbi, "spectral-co-clustering", k=5, seed=0,
                                      source_entity_type="document", metrics_to_compute=None)
