#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 24 12:04:45 2020

@author: alexis
"""

import networkx as nx
from cdlib import algorithms


def cdlib_handler(algorithm_name):
    cdlib_function = algorithms.__dict__[algorithm_name]
    return cdlib_function


if __name__ == "__main__":
    G = nx.karate_club_graph()
    coms = algorithms.infomap(G)
    coms2 = algorithms.louvain(G)
    
    

