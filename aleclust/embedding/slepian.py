#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 15:40:49 2020

@author: raphaelliegeois
"""

import numpy as np
import scipy as sc

def my_flip(U):
    U2 = np.copy(U)
    for i in range(0,len(U)):
        id = np.argmax(np.absolute(U[:,i]))
        if np.sign(U[id,i])<0:
            U2[:,i] = - U[:,i]
    return U2

def normalize(A,CONST_NORMALIZE):
    msize = len(A)
    if CONST_NORMALIZE:
        D = A @ np.ones((msize,1))
        nd = np.power(D,-0.5)
        n = np.diag(nd.flatten())
        An = n @ A @ n
        Dn = np.diag((An @ np.ones((msize,1))).flatten())
    else:
        An = np.copy(A)
        Dn = np.diag((An @ np.ones((msize,1))).flatten())
    return An,Dn

def slepian_embedding(M_adj, M_coop=None, n_components=2):
    '''
    M  : adjacency matrix
    M_coop : cooperation matrix
    '''
    if M_coop is None:
        M_coop = np.eye(M_adj.shape[0]) 

    [A,D] = normalize(M_adj,1)
    L = D - A
    
    M2 = np.real(M_coop - (sc.linalg.sqrtm(L) @ M_coop @ sc.linalg.sqrtm(L)))
    
    [Zeta,V] = np.linalg.eig((M2+np.transpose(M2))/2)
    idx = np.argsort(Zeta)[::-1]
    Zeta = Zeta[idx]
    V = V[:,idx]
    Mu = np.real(np.diag((np.transpose(V) @ M_coop @ V))/np.diag((np.transpose(V) @ V)))
    Xi = np.real(np.diag((np.transpose(V) @ sc.linalg.sqrtm(L) @ M_coop @ sc.linalg.sqrtm(L) @ V))/np.diag((np.transpose(V) @ V)))
    V=my_flip(V)  
    
    # select first n_components
    V = V[:, :n_components]
    
    #return V,Zeta,Mu,Xi
    return V
    