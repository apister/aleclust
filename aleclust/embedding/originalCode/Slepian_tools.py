#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 15:40:49 2020

@author: raphaelliegeois
"""

import numpy as np
import scipy as sc

def my_flip(U):
    U2 = np.copy(U)
    for i in range(0,len(U)):
        id = np.argmax(np.absolute(U[:,i]))
        if np.sign(U[id,i])<0:
            U2[:,i] = - U[:,i]
    return U2

def normalize(A,CONST_NORMALIZE):
    msize = len(A)
    if CONST_NORMALIZE:
        D = A @ np.ones((msize,1))
        nd = np.power(D,-0.5)
        n = np.diag(nd.flatten())
        An = n @ A @ n
        Dn = np.diag((An @ np.ones((msize,1))).flatten())
    else:
        An = np.copy(A)
        Dn = np.diag((An @ np.ones((msize,1))).flatten())
    return An,Dn

def get_Slepian(M2,S,L):
    [Zeta,V] = np.linalg.eig((M2+np.transpose(M2))/2)
    idx = np.argsort(Zeta)[::-1]
    Zeta = Zeta[idx]
    V = V[:,idx]
    Mu = np.real(np.diag((np.transpose(V) @ S @ V))/np.diag((np.transpose(V) @ V)))
    Xi = np.real(np.diag((np.transpose(V) @ sc.linalg.sqrtm(L) @ S @ sc.linalg.sqrtm(L) @ V))/np.diag((np.transpose(V) @ V)))
    V=my_flip(V)  
    return V,Zeta,Mu,Xi
    