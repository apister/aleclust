#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 14:11:35 2020

@author: Raphael Liegeois
"""

import numpy as np
import scipy.io as sio
import scipy as sc
from Slepian_tools import my_flip,normalize,get_Slepian

# Loading data (provided by Dr. Petra Vertes/Bullmore Lab/Cambridge)

mat_content = sio.loadmat('Worm279dir.mat')

A = mat_content['Worm279_matrix']
A = (np.transpose(A)+A)/2
CONST_NUM_NODES = 279 # Number of nodes in the C. elegans graph

# Create normalized A, L and B
[A,D] = normalize(A,1)
L = D - A
B = A - np.reshape(np.diag(D),(CONST_NUM_NODES,1)) @ np.reshape(np.diag(D),(1,CONST_NUM_NODES))/np.trace(D) #modularity matrix

# Eigenspectrum of A
[Lambda_Adj,U_Adj] = np.linalg.eig(A)
idx = np.argsort(Lambda_Adj)[::-1]
Lambda_Adj = Lambda_Adj[idx]
U_Adj = U_Adj[:,idx]
U_Adj = my_flip(U_Adj)

# Eigenspectrum of L
[Lambda_Lap,U_Lap] = np.linalg.eig(L)
idx = np.argsort(Lambda_Lap)
Lambda_Lap = Lambda_Lap[idx]
U_Lap = U_Lap[:,idx]
U_Lap = my_flip(U_Lap)

# Eigenspectrum of Modularity
[Lambda_Mod,U_Mod] = np.linalg.eig(B)
idx = np.argsort(Lambda_Mod)[::-1]
Lambda_Mod = Lambda_Mod[idx]
U_Mod = U_Mod[:,idx]
U_Mod = my_flip(U_Mod)

# Slepian Embedding
M = mat_content['M'] #Example cooperation matrix built when considering Sensory-Inter neurons
M2 = np.real(M - (sc.linalg.sqrtm(L) @ M @ sc.linalg.sqrtm(L)))
[V,Zeta,Mu,Xi] = get_Slepian(M2,M,L)

# ADDED
#from aleclust.embedding.slepian import slepian_embedding
#V = slepian_embedding(A, M)

# Plotting results

import matplotlib.pyplot as plt
NODE_TYPE = mat_content['NODE_TYPE'].flatten()
#ADJACENCY
#Plotting spectrum
plt.figure()
plt.plot(Lambda_Adj)
plt.ylabel('Eigenvalue')
plt.xlabel('Index')
plt.title('Spectrum of normalized A')
#Plotting embedding
plt.figure()
plt.plot(U_Adj[np.where(NODE_TYPE==1),0],U_Adj[np.where(NODE_TYPE==1),1], 'ro')#Sensory
plt.plot(U_Adj[np.where(NODE_TYPE==2),0],U_Adj[np.where(NODE_TYPE==2),1], 'ko')#Motor
plt.plot(U_Adj[np.where(NODE_TYPE==3),0],U_Adj[np.where(NODE_TYPE==3),1], 'go')#Inter
plt.plot(U_Adj[np.where(NODE_TYPE>3),0],U_Adj[np.where(NODE_TYPE>3),1], 'bo')#Other
plt.ylabel('Eigenvector 2')
plt.xlabel('Eigenvector 1')
plt.title('Adjacency Embedding')

#LAPLACIAN
#Plotting spectrum
plt.figure()
plt.plot(Lambda_Lap)
plt.ylabel('Eigenvalue')
plt.xlabel('Index')
plt.title('Spectrum of normalized L')
#Plotting embedding
plt.figure()
plt.plot(U_Lap[np.where(NODE_TYPE==1),1],U_Lap[np.where(NODE_TYPE==1),2], 'ro')#Sensory
plt.plot(U_Lap[np.where(NODE_TYPE==2),1],U_Lap[np.where(NODE_TYPE==2),2], 'ko')#Inter
plt.plot(U_Lap[np.where(NODE_TYPE==3),1],U_Lap[np.where(NODE_TYPE==3),2], 'go')#Motor
plt.plot(U_Lap[np.where(NODE_TYPE>3),1],U_Lap[np.where(NODE_TYPE>3),2], 'bo')#Other
plt.ylabel('Eigenvector 3')
plt.xlabel('Eigenvector 2')
plt.title('Laplacian Embedding')
plt.axis([-0.10, 0.16, -0.10, 0.10])

#MODULARITY
#Plotting spectrum
plt.figure()
plt.plot(Lambda_Mod)
plt.ylabel('Eigenvalue')
plt.xlabel('Index')
plt.title('Spectrum of normalized B')
#Plotting embedding
plt.figure()
plt.plot(U_Mod[np.where(NODE_TYPE==1),0],U_Mod[np.where(NODE_TYPE==1),1], 'ro')#Sensory
plt.plot(U_Mod[np.where(NODE_TYPE==2),0],U_Mod[np.where(NODE_TYPE==2),1], 'ko')#Inter
plt.plot(U_Mod[np.where(NODE_TYPE==3),0],U_Mod[np.where(NODE_TYPE==3),1], 'go')#Motor
plt.plot(U_Mod[np.where(NODE_TYPE>3),0],U_Mod[np.where(NODE_TYPE>3),1], 'bo')#Other
plt.ylabel('Eigenvector 2')
plt.xlabel('Eigenvector 1')
plt.title('Modularity Embedding')

#SLEPIAN
#Plotting spectrum
plt.figure()
plt.plot(Zeta,label="Composite Criterion")
plt.plot(Xi,'r',label="Energy Concentration")
plt.plot(Mu,'g',label="Modified Emd. Dist.")
plt.legend(loc="upper left")
plt.ylabel('Eigenvalue')
plt.xlabel('Index')
plt.title('Spectrum of modiufied criterion')
#Plotting embedding (Zooming on everything except Motor, i.e., green dots)
plt.figure()
plt.plot(V[np.where(NODE_TYPE==1),0],V[np.where(NODE_TYPE==1),1], 'ro')#Sensory
plt.plot(V[np.where(NODE_TYPE==2),0],V[np.where(NODE_TYPE==2),1], 'ko')#Inter
plt.plot(V[np.where(NODE_TYPE==3),0],V[np.where(NODE_TYPE==3),1], 'go')#Motor
plt.plot(V[np.where(NODE_TYPE>3),0],V[np.where(NODE_TYPE>3),1], 'bo')#Other
plt.ylabel('Eigenvector 2')
plt.xlabel('Eigenvector 1')
plt.title('Modified Embedding')






