# -*- coding: utf-8 -*-
"""
Created on Mon Oct 28 14:38:06 2019

@author: Aviz
"""
import networkx as nx
import numpy as np
import rapidjson

import aleclust.processing.preprocessing as preprocessing
import aleclust.clustering.clustering as clustering
import aleclust.utility as utility
import aleclust.clustering.metric as metric


def jaccard_similarity(list1, list2):
    intersection = len(list(set(list1).intersection(list2)))
    union = (len(list1) + len(list2)) - intersection
    return float(intersection) / union

def sortTimeSlots(allTimeSlots, convert=True):
    """
        from a set of timeslot, convert it into a list of integers and sort it
    """
    if convert:
        # convert to integers
        allTimeSlots = list(map(lambda e: int(e), allTimeSlots))
    
    allTimeSlots.sort()
    return allTimeSlots

def getAllTimeSlots(G, timeslot_key="w"):
    """Return all timeslots from graph with times of the form {w : {2013:1, 2012:2}} in each edge"""
    allTimeSlots = set()
    
    edges = G.edges.data()
    allTs = [t[timeslot_key] for u,v,t in edges]
    
    for times in allTs:
        for ts in times:
            if ts not in allTimeSlots:
                allTimeSlots.add(ts)
            
    return allTimeSlots


def biGetAllTimeSlots(G, timeslot_key="ts", entity_type_key="entity_type", timestamped_entity_type="publication"):
    """
    Return all possible timeslots of a bipartite graph by iterating over all publication (set2) nodes
    """
    timeslots = [a[timeslot_key] for n,a in G.nodes.data() if a[entity_type_key] == timestamped_entity_type]
    timeslots_set = set(timeslots)
    return timeslots_set
    

def biGraphToSnapshot(G, time_key="ts", entity_type_key="entity_type", timestamped_entity_type="publication"):
    """
    Create Snapshot graph from bipartite graph. Each snapshot is a graph which only have the publications of a specific timeslot, with  his neighbors
    """
    timeslots = biGetAllTimeSlots(G, timestamped_entity_type = timestamped_entity_type)
    timeslots = sortTimeSlots(timeslots)
    
    SG = {i:nx.Graph() for i in timeslots}
    
    for n,a in [(n,a) for n,a in G.nodes.data() if a[entity_type_key] == timestamped_entity_type]:
        ts = int(a[time_key])
        
        # add publication node
        SG[ts].add_node(n, **a)
        
        # add all his neighbors
        for neighbor in G[n]:
            SG[ts].add_node(neighbor, **G.nodes[neighbor]) #add node with attributes
            SG[ts].add_edge(n, neighbor) #add edge
    
    return SG


# TODO : for now, we compute all time slots directly from the graph, but it would be best to already have this information before
def graphToSnapshot(G, times_weight_key="w"):
    """
    transform a graph which edge have times attributs of the shape {w : {2013:1, 2012:2}} in a list of graph. One graph is made for each timeslot. In a timeslot, remove the nodes without any connection (need a change maybe)
    """
    allTimeSlots = getAllTimeSlots(G)
    allTimeSlots = sortTimeSlots(allTimeSlots)
    
    graph = nx.Graph()
    graph.add_nodes_from(G.nodes.data())
    
    SG = {i:nx.Graph(graph) for i in allTimeSlots}
    
    # Iterate over the edges of the overall graph and add them to the different snapshots 
    for edge in G.edges.data():
        for timeSlot in edge[2][times_weight_key]:
            SG[int(timeSlot)].add_edge(*edge[:-1]) #, times_weight_key = edge[2][times_weight_key][timeSlot])
            SG[int(timeSlot)].edges[(edge[:-1])].update({times_weight_key : edge[2][times_weight_key][timeSlot]})
            
            
    # remove nodes which dont have any connection
    for snapshot in SG.values():
        snapshot.remove_nodes_from(list(nx.isolates(snapshot)))
        
    return SG


def compute_distance_matrix(partition1, partition2, distanceFunction):
    """
        Compute the distance matrix between the communities of two different partition of the same graph. partition1 will be in row and partition 2 in columns
    """
    distance_matrix = np.empty([len(partition1), len(partition2)])
    for i,staticCommunity in enumerate(partition1): # loop communities of partition 1
        for j,staticCommunity2 in enumerate(partition2): # loop communities of partition 2
            distance = distanceFunction(staticCommunity, staticCommunity2)
            distance_matrix[i][j] = distance
    return distance_matrix


def fill_communities_id(communities_ids_t0, communities_ids_t1, distance_matrix, max_id):
    """
        Fill the ids of communities t+1 with communities ids of t and the distance matrix. Each community of t+1 can only be linked to one community of t. If there is no match with some community of t+1, give them a new id
    """
    # Get the maximum similarity to match 2 communities. At each match, erase the column and row of the max. This way, each community of t+1 can only be linked to one community of t
    while 1 not in distance_matrix.shape:
        max_inds = np.unravel_index(np.argmax(distance_matrix, axis=None), distance_matrix.shape)
        communities_ids_t1[max_inds[0]] = communities_ids_t0[max_inds[1]]
        distance_matrix = np.delete(distance_matrix, max_inds[0], 0) #delete row
        distance_matrix = np.delete(distance_matrix, max_inds[1], 1) #delete row
    
    # give new ids when there is no match
    for i,com_id in enumerate(communities_ids_t1):
        if com_id == -1:
            communities_ids_t1[i] = max_id + 1
            max_id += 1
    
    return max_id


# TODO : make a class maybe in the future. Use more optimized jaccard similarity function
# TODO : better handler of the timeslot key between uni and bipartite graphs
# TODO : Handle bipartite metrics
def instantOptimalSimple(G, clusteringAlgorithm, distanceFunction = jaccard_similarity, distanceThreshold = 0.4, node_communities_key_name="communities", times_weight_key = "w", metrics = ["coverage", "performance", "modularity"], metrics_args=None, bipartite=False, timestamped_entity_type="publication",  **kwargs):
    """
    Instant optimal dynamic community detection. 
    """
    #print("instant optimal graph input : ")
    #print(G.nodes.data())
    #print(G.edges.data())
    
    utility.add_list_to_all_nodes(G, attribute_name=node_communities_key_name, attribute_value=[])
    
    if bipartite:
        snapshotGraph = biGraphToSnapshot(G, time_key="ts", timestamped_entity_type=timestamped_entity_type)
    else:
        snapshotGraph = graphToSnapshot(G, times_weight_key=times_weight_key)
        
    snapshotGraph = [*snapshotGraph.values()] # save only graph objects, remove timeslots
    
    partitionList = []
    
    if metrics != None and metrics != "None":
        metrics_overall = {m:[] for m in metrics}
    
    for t,graph in enumerate(snapshotGraph):
        #print(t)
        
        # Verify constraints of algorithms
        if clusteringAlgorithm == "fluid" and not(nx.is_connected(graph)):
            return "The fluid community detection algorithm requires a connected graph."

        # apply static community detection on snapshot Gt
        #print("apply clustering on SG")
        partition = clustering.applyClusteringAlgorithm(graph, clusteringAlgorithm, **kwargs).values()
        partitionList.append(partition)
        
        #TODO : If no community are found in a timeslot the function currently abort. It would take too much time for now to take into account this case, to do later if needed 
        if list(partition) == []:
            print("no communities found, abortion of instant optimal scheme")
            return G
        
        # Compute metrics
        if metrics != None and metrics != "None":
            #print("compute metrics")
            for met_name in metrics:
                metResult = metric.compute_metric(graph, partition, met_name, **metrics_args)
                metrics_overall[met_name].append(metResult)
        
        if t == 0:
            # ids of communities of time 0
            #communities_ids = list(range(len(partition)))
            communities_ids_t1 = list(range(len(partition)))
            max_id = len(communities_ids_t1) - 1
        else:
            communities_ids_t0 = communities_ids_t1
            communities_ids_t1 = [-1 for n in range(len(partition))]
            
            # compute the distance matrix between each pair of communities of times t and t - 1
            #print("compute distance matrix")
            distance_matrix = compute_distance_matrix(partitionList[t], partitionList[t-1], distanceFunction = distanceFunction)
            #print("link communities of t with communities of t-1")
            max_id = fill_communities_id(communities_ids_t0, communities_ids_t1, distance_matrix, max_id)
                    
        # set the community id for each node at each timeslot
        #print("set communities id into graph's nodes community key")
        for i, comm in enumerate(partition):
            for node in comm:
                #print(G.node[node][node_communities_key_name])
                if not(G.nodes[node][node_communities_key_name]) or len(G.nodes[node][node_communities_key_name]) == 0:
                    G.nodes[node][node_communities_key_name] = [[communities_ids_t1[i]]]
                elif len(G.nodes[node][node_communities_key_name]) < t + 1:
                    G.nodes[node][node_communities_key_name].append([communities_ids_t1[i]])
                else:
                    G.nodes[node][node_communities_key_name][t].append(communities_ids_t1[i])
                
        nodes_with_community = set().union(*partition)
        nodes_without_community = G.nodes() - nodes_with_community
        
        # append "none" if a node is not part of any community at timeslot t
        for n in nodes_without_community:
            G.nodes[n][node_communities_key_name].append("None")
            
    # transform nested list of communities of len 1 into their element : [1,[3],[4,10]] => [1,3,[4,10]]
    for n in G.nodes:
        G.nodes[n][node_communities_key_name] = utility.del_nested_list(G.nodes[n][node_communities_key_name])
    
    if metrics != "None" and metrics != None:
        G.graph["metrics"] = {}
        # Average the metrics over time
        for m_name, metric_list in metrics_overall.items():
            average_m = sum(metric_list) / len(metric_list)
            G.graph["metrics"][m_name + "Average"] = average_m
        
        # keep the metrics of each timeslot in the metadata
        for m in metrics_overall:
            metrics_overall[m + "TimeSlots"] = metrics_overall.pop(m)

        G.graph["metrics"].update(metrics_overall)
        G.graph = {**G.graph}
            
    return G


#%%
if __name__ == "__main__":
    # instant optimal only work with new json format
    avizDataPath = "../data/paohvis/aviz.json"
    avizJson = rapidjson.loads(open(avizDataPath, "r").read())
    Gaviz = preprocessing.json_to_graph(avizJson)
    instantOptimalSimple(Gaviz, clusteringAlgorithm=clustering.applyClusteringAlgorithm, clusteringAlgo="percolation", times_weight_key="w", k=3)

    
    
    #%% ONE MODE GRAPH
    infoVisDataPath = "../data/new_json/IEEE_VIS_2012_2016_InfoVis.json"
    json_data = rapidjson.loads(open(infoVisDataPath).read())
    G = preprocessing.json_to_graph(json_data)

    G = instantOptimalSimple(G, clusteringAlgorithm="girvan-newman")
    
    #%%
    test_path = "../data/new_json/test.json"
    G_test = preprocessing.path_to_graph(test_path, projection=True, projection_set="person")
    G_test = instantOptimalSimple(G_test, clusteringAlgorithm="fluid", metrics="None", k=2)
    
    #%% TWO MODE GRAPH
    Gbi = preprocessing.path_to_graph(api.dataPaths["IEEE_VIS"], projection=False)
    SGbi = biGraphToSnapshot(Gbi)
    
    Gbi = instantOptimalSimple(Gbi, clusteringAlgorithm="spectralCoClustering", metrics=None, bipartite=True)
    
    #%%
    test_path = "../data/new_json/test.json"
    G_test_bi = preprocessing.path_to_graph(test_path, projection=False)
    G_test_bi = instantOptimalSimple(G_test_bi, clusteringAlgorithm="spectral-co-clustering", metrics="None", timestamped_entity_type="contract", bipartite=True, source_entity_type="person")
    
