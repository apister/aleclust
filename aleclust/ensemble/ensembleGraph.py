#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr  5 10:20:10 2020

@author: alexis
"""
import networkx as nx

import aleclust.ensemble.pk_clustering as ensemble
import aleclust.processing.preprocessing as preprocessing
import aleclust.processing.projection as projection
import aleclust.clustering.clustering as clustering


class EnsembleGraph:
    def __init__(self, ensemble_cd):
        self.ensemble: ensemble.PkClustering = ensemble_cd
        self.G_uni = self.ensemble.G_uni

        self.bipartite_nodes_clusters = None
        self.N_algorithms = None
        self.ensemble_graph = None
        self.consensus = None
        self.ensemble_results = None

    def build_bipartite(self, algorithms_list=None):
        """Build hypergraph (represented as bipartite) of ensemble clustering : persons as nodes and every cluster (
        from every clustering) is an hyperedge """
        if algorithms_list is None:
            algorithms_list = self.ensemble.algorithms_names
        self.N_algorithms = len(algorithms_list)

        print('build hypergraph of ensemble object')
        self.bipartite_nodes_clusters = nx.Graph()
        self.bipartite_nodes_clusters.add_nodes_from(self.G_uni.nodes, entity_type='person')

        # iterate over clustering results of algorithms in algorithms_list
        for i, (algorithm_name, clustering) in enumerate(
                [(alg_name, node_clustering) for alg_name, node_clustering in self.ensemble.all_clusterings.items() if
                 alg_name in algorithms_list]):
            for j, cluster in enumerate(clustering.communities):
                cluster_id = 'c_' + str(i) + '_' + str(j)
                self.bipartite_nodes_clusters.add_node(cluster_id, entity_type='cluster',
                                                       algorithm=clustering.algorithm_name)
                for node_id in cluster:
                    # TODO : some bipartite CD algos give a community to the 2 sets of nodes, have an option to ouptut only one type in the future
                    if node_id in self.G_uni.nodes:
                        self.bipartite_nodes_clusters.add_edge(node_id, cluster_id)

        return self.bipartite_nodes_clusters

    def build_ensemble_graph(self):
        self.ensemble_graph = projection.weighted_projection(self.bipartite_nodes_clusters, projection_set='person',
                                                             secondary_set='cluster', is_time=False)

    def find_cliques_min_weight(graph, weight_key='w', min_weight=1):
        """Find cliques where each edge have a weight minimal"""
        print('find cliques')
        G = graph.copy()
        edges_to_delete = []
        for u, v, w in G.edges.data(weight_key):
            if w < min_weight:
                edges_to_delete.append((u, v))
        G.remove_edges_from(edges_to_delete)

        # find components
        cliques = nx.algorithms.clique.find_cliques(G)
        cliques_filt = [c for c in cliques if len(c) > 1]
        cliques_sorted = sorted(cliques_filt, key=len, reverse=True)

        print(cliques_sorted)
        #        sorted_components = [c for c in sorted(nx.connected_components(G), key=len, reverse=True)]
        return cliques_sorted

    def find_consensus(self):
        min_weight = self.N_algorithms
        self.consensus = EnsembleGraph.find_cliques_min_weight(self.ensemble_graph, min_weight=min_weight)

    def consensus_to_json(self):
        """transform consensus into json format to send it"""
        json = {}
        for i, consensus in self.consensus:
            consensus_id = 'c_' + str(i)
            json[consensus_id] = {
                'nodes': consensus,
                'N_nodes': len(consensus)
            }
        return json

    def ensemble_clustering(self, meta_cd_algo="louvain", weight="w", **kwargs):
        self.build_bipartite()
        self.build_ensemble_graph()
        self.ensemble_results = clustering.applyClusteringAlgorithm(self.ensemble_graph, meta_cd_algo, weight=weight,
                                                                    **kwargs)

    def match_algorithms_with_ensemble(self):
        self.ensemble.prior = self.ensemble_results.communities_dict
        self.ensemble.ensemble_clustering = self.ensemble_results
        self.ensemble.ensemble_clustering_json = self.ensemble_results.to_json()
        self.ensemble.match_all_algorithms()

    def add_ensemble_to_other_clusterings(self):
        self.ensemble.all_clusterings_json['ensemble']["graph"]['ensemble_prior_knowledge'] = \
            self.ensemble_results.communities_dict
        # self.ensemble.all_clusterings_json['ensemble']['new_prior_knowledge_names'] = \
        #     self.ensemble_results.communities_dict_to_names(self.ensemble.json)
