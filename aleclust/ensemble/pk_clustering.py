# -*- coding: utf-8 -*-
"""
Created on Wed Jan 22 17:17:08 2020

@author: Aviz
"""

import numpy as np
import networkx as nx
from sklearn.model_selection import ParameterGrid

import aleclust.clustering.clustering as clustering
import aleclust.processing.preprocessing as preprocessing
import aleclust.utility as utility
import aleclust.clustering.semiSupervised as semiSupervised
from aleclust.clustering.communities import Communities
from aleclust.globals import UNIPARTITE_CDA, STATIC_CDA, BIPARTITE_CDA, ATTRIBUTE_UNIPARTITE_CDA
from aleclust.processing.paohJsonParser import PaohJsonParser
from aleclust.processing.parameters import Parameters

from py_stringmatching.similarity_measure.overlap_coefficient import OverlapCoefficient
import functools
import timeout_decorator


def overlap_coeff(set1, set2):
    if type(set1) not in (set, list):
        set1 = list(set1)

    if type(set2) not in (set, list):
        set2 = list(set2)

    oc = OverlapCoefficient()
    return oc.get_raw_score(set1, set2)


def len_intersection(lst1, lst2):
    return len(set(lst1).intersection(lst2))


# TODO : create a function to take default parameters for ensemble here AND preprocessing of API
# TODO : handling of global parameters
class PkClustering:
    def __init__(self, json_data, algorithms_names=None, parameters_dict={}, parameters_grid=None,
                 pk_key="prior_knowledge"):
        self.json = json_data
        if algorithms_names is None:
            algorithms_names = ["guimera", "greedy-modularity", "spectral-co-clustering", "girvan-newman", "louvain",
                                "fluid",
                                "label-propagation", "walktrap", "em"]

        self.algorithms_names = algorithms_names
        self.parameters_dict = parameters_dict
        self.pk_key = pk_key

        self._prior = None
        self.n_pk = None
        self.n_cluster_init = None
        self.run_semi_supervised = None
        self.parse_json()

        self.prior_list = list(self.prior.values())

        if parameters_grid is None:
            parameters_grid = {alg: {'k': [self.n_cluster_init, self.n_cluster_init + 1, self.n_cluster_init + 2]} for
                               alg in
                               self.algorithms_names if
                               'k' in STATIC_CDA[alg]['parameters']}
        self.parameters_grid = parameters_grid

        self.json_parser = PaohJsonParser(self.json)
        self.parameters: Parameters = self.json_parser.parameters

        # get uni and bipartite graphs from json
        self.G_uni = None
        self.G_bi = None
        self.compute_graphs()

        # dicts of algo name : json result
        self.all_clusterings_json = {}
        self.topological_clusterings_json = {}
        self.attribute_clusterings_json = {}
        self.ss_clusterings_json = {}
        self.ensemble_clustering_json = None

        # dicts of algo name : NodeClustering objects
        self.all_clusterings = {}
        self.topological_clusterings = {}
        self.attribute_clusterings = {}
        self.ss_clusterings = {}
        self.ensemble_clustering = None

        # TODO : REDO THIS
        self.isolated_nodes_uni = None
        self.isolated_nodes_bi = None

        self.id_for_algorithm = None

        # communities id linked to the PK groups for each algorithm
        self.pk_communities_ids = None

    def parse_json(self):
        if self.pk_key in self.json['metadata'] and self.json['metadata']['prior_knowledge'] != {}:
            self.prior = self.json['metadata'][self.pk_key]
            self.n_cluster_init = self.n_pk
            self.run_semi_supervised = True
        else:
            self.prior = {}
            self.n_cluster_init = 2
            self.run_semi_supervised = False
        self.n_pk = len(self.prior)

    @property
    def prior(self):
        return self._prior

    @prior.setter
    def prior(self, new_prior):
        if type(new_prior) == dict:  # type checking
            self._prior = new_prior
            self.n_pk = len(self.prior)
            self.prior_list = list(self.prior.values())
        else:
            raise Exception("Invalid value for name")

    def set_default_parameters(self):
        """
        Take the default parameters values for the parameters not precised of each algorithm
        """
        for i, alg_name in enumerate(self.algorithms_names):
            if alg_name not in self.parameters_dict:
                self.parameters_dict[alg_name] = {}

        self.parameters_dict[alg_name] = self.parameters.set_default_clustering_parameters(alg_name)

    def get_default_parameters(self, alg_name):
        if STATIC_CDA[alg_name]["parameters"] in ("None", None):
            parameters = {}
        else:
            parameters = {param: STATIC_CDA[alg_name]['parameters'][param]['default']
                          for param in STATIC_CDA[alg_name]['parameters']}

        if alg_name == 'guimera':
            parameters["clustered_set"] = self.target_entity_type
        elif alg_name == "spectral-co-clustering":
            parameters["source_entity_type"] = self.source_entity_type
        return parameters

    def compute_graphs(self):
        self.G_uni = PaohJsonParser.json_to_graph(self.json, projection=True,
                                                  projection_set=self.parameters.projection_set)
        self.G_bi = PaohJsonParser.json_to_graph(self.json, projection=False, projection_set=None)

        # print('N NODES : ', len(self.G_uni))
        # print('N hyper EDGES : ', len([u for u, at in self.G_bi.nodes.data()
        #                                if at['entity_type'] == self.parameters.source_entity_type]))
        # print('N EDGES : ', len(self.G_uni.edges))

        # remove isolated nodes (bipartite graph are not supposed to have any isolated nodes)
        self.isolated_nodes_uni = preprocessing.remove_isolated_nodes(self.G_uni)
        self.isolated_nodes_bi = preprocessing.remove_isolated_nodes(self.G_bi)

    # TODO : Transform into class
    def match_prior(self, partition, match_function=len_intersection):
        """
        Match a partition found by a clustering algorithm with a prior
        knowledge. Do this by creating a bipartite graph with the PK
        groups as a node set and the clusters as the other node set
        and finding a max weight matching. Return the edit distance
        and the matching result for each PK group in a dictionary
        """
        # names of prior are converted to strings to not have the same than the partition
        priors = {str(name): prr for name, prr in self.prior.items()}

        # we encode communities names to integers ids, to not have the same values for clusters and PK groups
        id_for_com = {i: v for i, v in enumerate(partition.keys())}
        com_for_id = {id_for_com[i]: i for i in range(len(id_for_com))}

        # bipartite matching graph
        G_matching = nx.Graph()
        G_matching.add_nodes_from(partition.keys())

        for prior_id, prior in priors.items():
            G_matching.add_node(prior_id)
            for com_name, com in partition.items():
                match = len_intersection(prior, com)

                if match > 0:
                    com_id = com_for_id[com_name]
                    G_matching.add_edge(com_id, prior_id, weight=match)

        max_matching = nx.algorithms.max_weight_matching(G_matching)
        pk_matching = {"matching": {}}

        total_weight = 0
        for edge in max_matching:
            # name of the PK group
            pk_id = (set(priors.keys()).intersection(set(edge))).pop()
            #            print("sets : ", set(edge), set(pk_id), set(edge) - set(pk_id))
            com_id = (set(edge) - set([pk_id])).pop()
            #            print(edge, 'pk', pk_id, ' com', com_id)

            weight = G_matching.edges[edge]["weight"]
            total_weight += weight

            pk_group_match = {"community": id_for_com[com_id],
                              "nMatchedNodes": weight,
                              "nNonMatchedNodes": len(priors[pk_id]) - weight,
                              "nOtherCommunityNodes": len(partition[id_for_com[com_id]]) - weight}
            pk_matching["matching"][pk_id] = pk_group_match

        edit_distance = utility.nested_list_num_elements(self.prior_list) - total_weight
        pk_matching["edit_distance"] = edit_distance

        if len(max_matching) != len(self.prior):
            return {"edit_distance": edit_distance, "matching": None}

        return pk_matching

    @timeout_decorator.timeout(7, use_signals=False)
    def run_one_algorithm(self, alg_name, parameters):
        if parameters in (None, "None"):
            parameters = {}

        if alg_name in UNIPARTITE_CDA:
            clustering_result = clustering.applyClusteringAlgorithm(self.G_uni, alg_name, self.isolated_nodes_uni,
                                                                    **parameters)
        elif alg_name in BIPARTITE_CDA:
            clustering_result = clustering.applyClusteringAlgorithm(self.G_bi, alg_name, self.isolated_nodes_bi,
                                                                    **parameters)
        else:
            raise Exception("Algorithm name not matching")

        return clustering_result

    def save_node_clustering(self, alg_name, node_clustering):
        # alg_name can be anything here
        matching = self.match_prior(node_clustering.communities_dict)

        result_dict = node_clustering.to_json()
        result_dict['graph']["prior_knowledge"] = matching
        result_dict['graph']['algorithm_type'] = 'topological'

        self.topological_clusterings[alg_name] = node_clustering
        self.topological_clusterings_json[alg_name] = result_dict

    def run_algorithms(self, grid_search=False):
        self.topological_clusterings_json = {}

        for alg_name in self.algorithms_names:
            print(alg_name)
            try:
                if not grid_search:
                    parameters_dict = self.parameters_dict[alg_name]
                    node_clustering = self.run_one_algorithm(alg_name, parameters_dict)
                    self.save_node_clustering(alg_name, node_clustering)
                else:
                    if alg_name not in self.parameters_grid or self.parameters_grid[alg_name] in (None, "None"):
                        # parameters = self.get_default_parameters(alg_name)
                        node_clustering = self.run_one_algorithm(alg_name, self.parameters_dict[alg_name])
                        self.save_node_clustering(alg_name, node_clustering)
                    else:
                        # fill the parameter grid with the parameters not precised using only the default value
                        parameters_not_precised = [param for param in STATIC_CDA[alg_name]['parameters'] if
                                                   param not in self.parameters_grid[alg_name]]
                        for param in parameters_not_precised:
                            if param == "source_entity_type":
                                param_value = self.parameters.source_entity_type
                            elif param == "target_entity_type":
                                param_value = self.parameters.target_entity_type
                            else:
                                param_value = STATIC_CDA[alg_name]['parameters'][param]['default']
                            self.parameters_grid[alg_name][param] = [param_value]

                        # # don't add the dependant parameters in the name of result
                        # for param in self.dependant_parameters:
                        #     parameters_not_precised.append(param)

                        # run parameter grid
                        grid = ParameterGrid(self.parameters_grid[alg_name])
                        for param_set in grid:
                            alg_name_params = alg_name
                            for param, value in param_set.items():
                                if param not in parameters_not_precised:
                                    alg_name_params += '_' + param + str(value)

                            node_clustering = self.run_one_algorithm(alg_name, param_set)
                            self.save_node_clustering(alg_name_params, node_clustering)
            except Exception as e:
                print('Error for ', alg_name, " : ", e)

    def match_one_algorithm(self, alg_name, node_clustering, alg_type):
        matching = self.match_prior(node_clustering.communities_dict)
        result_dict = node_clustering.to_json()
        result_dict['graph']["prior_knowledge"] = matching
        result_dict['graph']['algorithm_type'] = alg_type

        if alg_type == 'topological':
            self.topological_clusterings_json[alg_name] = result_dict
        elif alg_type == 'attribute':
            self.attribute_clusterings_json[alg_name] = result_dict
        elif alg_type == 'propagation':
            self.ss_clusterings_json[alg_name] = result_dict
        elif alg_type == 'ensemble':
            self.ensemble_clustering_json = result_dict

    def match_all_algorithms(self):
        for alg_name, node_clustering in self.topological_clusterings.items():
            self.match_one_algorithm(alg_name, node_clustering, "topological")
        for alg_name, node_clustering in self.attribute_clusterings.items():
            self.match_one_algorithm(alg_name, node_clustering, "attribute")
        for alg_name, node_clustering in self.ss_clusterings.items():
            self.match_one_algorithm(alg_name, node_clustering, "propagation")

        if self.ensemble_clustering is not None:
            self.match_one_algorithm("ensemble", self.ensemble_clustering, "ensemble")

    def parse_attribute(self, attribute):
        """ receive attribute in the format att_type (int or string) """
        ind = attribute.rfind('_')
        att_name = attribute[:ind]
        att_type = attribute[ind + 1:]
        return att_name, att_type

    # @timeout_decorator.timeout(20, use_signals=False)
    def run_attribute_algorithms(self, attributes):
        import random as rn

        rn.seed(0)
        np.random.seed(0)

        print('attrs', attributes)
        for attribute in attributes:
            try:
                attr, attr_type = self.parse_attribute(attribute)
                print(attr, attr_type)
                if attr_type == 'int':
                    print('ilouvain')
                    algo_name = 'ilouvain' + '_' + attr
                    node_clustering = clustering.applyClusteringAlgorithm(self.G_uni, 'ilouvain',
                                                                          self.isolated_nodes_uni, attributes=[attr])
                elif attr_type == 'string':
                    algo_name = 'eva' + '_' + attr
                    node_clustering = clustering.applyClusteringAlgorithm(self.G_uni, 'eva', self.isolated_nodes_uni,
                                                                          attributes=[attr])
                else:
                    raise Exception('attribute type must be int or string')

                node_clustering.algorithm_name = algo_name
                self.algorithms_names.append(algo_name)
                self.attribute_clusterings[algo_name] = node_clustering

            except Exception as e:
                print("error for " + attr)
                print(e)

    # TODO : redo better
    def run_semi_supervised_algorithms(self):
        """
        Run the semi supervised algorithms and match with the prior knowledge. The natching will always be perfect
        """
        coms_propagation = semiSupervised.labelPropagation_handler(self.G_uni, self.prior,
                                                                   algorithm='label-propagation')

        node_clustering = Communities(coms_propagation, self.G_uni, 'label-propagation-ss', self.isolated_nodes_uni)
        self.ss_clusterings['label-propagation-ss'] = node_clustering

        coms_spreading = semiSupervised.labelPropagation_handler(self.G_uni, self.prior, algorithm='label-spreading')
        node_clustering_spreading = Communities(coms_spreading, self.G_uni, 'label-spreading-ss',
                                                self.isolated_nodes_uni)
        self.ss_clusterings['label-spreading-ss'] = node_clustering_spreading

    def get_alg_result(self, alg_name):
        """
        get clustering result by alg name
        """
        i = np.where(np.array(self.algorithms_names) == alg_name)[0][0]
        return self.topological_clusterings_json[i]

    def run(self, attributes=None, run_semi_supervised=True, grid_search=False):
        self.set_default_parameters()

        # Run CD algorithms
        self.run_algorithms(grid_search)

        print("ATTRIBUTES ", attributes)
        if attributes is not None:
            try:
                self.run_attribute_algorithms(attributes)
            except Exception as e:
                print('error for attributes algorithms : ', e)

        if run_semi_supervised:
            print('run semi supervised based algorithms')
            self.algorithms_names.append('label-spreading-ss')
            self.algorithms_names.append('label-propagation-ss')

            try:
                self.run_semi_supervised_algorithms()
            except Exception as e:
                print('error for semi supervised algorithms : ', e)

        self.all_clusterings = {**self.topological_clusterings, **self.attribute_clusterings, **self.ss_clusterings}

    def merge_results(self):
        self.all_clusterings_json = {**self.topological_clusterings_json,
                                     **self.attribute_clusterings_json,
                                     **self.ss_clusterings_json}
        if self.ensemble_clustering_json is not None:
            self.all_clusterings_json["ensemble"] = self.ensemble_clustering_json

        self.pk_communities_ids = [{alg_name: [pk_group['community'] for pk_group in
                                               clustering_result['graph']['prior_knowledge']['matching'].values()]} for
                                   alg_name, clustering_result in self.all_clusterings_json.items() if
                                   clustering_result['graph']['prior_knowledge']['matching'] is not None]
        print("ID ", self.pk_communities_ids)
        self.pk_communities_ids = functools.reduce(lambda a, b: {**a, **b}, self.pk_communities_ids)
        for alg_name in self.algorithms_names:
            if alg_name not in self.pk_communities_ids:
                self.pk_communities_ids[alg_name] = []

    def all_matching_from_algorithm(self, algorithm_name, algorithm_list):
        """Match a clustering with every other clustering one by one"""
        import copy

        reference_clustering = self.all_clusterings[algorithm_name]

        communities_ids = reference_clustering.communities_dict.keys()
        communities_total_shared_nodes = {com_id: 0 for com_id in communities_ids}

        result = {'results': {}}

        for alg_name in algorithm_list:
            node_clustering = self.all_clusterings[alg_name]
            nodes_classification = copy.deepcopy(self.all_clusterings_json[alg_name]['nodes'])

            matching = PkClustering.match_partitions(reference_clustering.communities_bitmap_dict,
                                                     node_clustering.communities_bitmap_dict)

            communities_total_shared_nodes = {k: communities_total_shared_nodes.get(k, 0) +
                                                 matching['n_shared_nodes'].get(k, 0)
                                              for k in
                                              set(communities_total_shared_nodes) | set(matching['n_shared_nodes'])}

            for node in nodes_classification:
                if node['community'] in matching['matching_results'].keys():
                    node['community'] = matching['matching_results'][node['community']]
                else:
                    node['community'] = None

            result['results'][alg_name] = {
                'matching': matching,
                'nodes_new_coms': nodes_classification
            }

        communities_consensus = {}
        for com_id in communities_ids:
            communities_consensus[com_id] = communities_total_shared_nodes[com_id] / \
                                            self.all_clusterings_json[algorithm_name]['graph']['communities'][com_id][
                                                'node_frequency']

        result['communities_consensus'] = communities_consensus

        return result

    def match_partitions(partition1, partition2):
        # we encode ccommunities ids with new ids for both partitions
        partition1_id_for_comid = {'p1_' + str(i): v for i, v in enumerate(partition1.keys())}
        partition1_comid_for_id = {v: i for i, v in partition1_id_for_comid.items()}

        partition2_id_for_comid = {'p2_' + str(i): v for i, v in enumerate(partition2.keys())}
        partition2_comid_for_id = {v: i for i, v in partition2_id_for_comid.items()}

        # bipartite matching graph
        G_matching = nx.Graph()
        G_matching.add_nodes_from(partition1_id_for_comid.keys())
        G_matching.add_nodes_from(partition2_id_for_comid.keys())

        for com_id, com in partition1.items():
            for com_id2, com2 in partition2.items():
                match = com.intersection_cardinality(com2)

                if match > 0:
                    G_matching.add_edge(partition1_comid_for_id[com_id], partition2_comid_for_id[com_id2], weight=match)

        max_matching = nx.algorithms.max_weight_matching(G_matching, weight='weight')

        matching_result = {}
        n_shared_nodes_by_com = {}

        for match in max_matching:
            com1_id = partition1_id_for_comid[(set(partition1_id_for_comid.keys()).intersection(set(match))).pop()]
            com2_id = partition2_id_for_comid[(set(partition2_id_for_comid.keys()).intersection(set(match))).pop()]

            matching_result[com2_id] = com1_id
            n_shared_nodes_by_com[com1_id] = G_matching.edges[match]['weight']

        for com2_id in partition2.keys():
            if com2_id not in matching_result:
                matching_result[com2_id] = None

        for com1_id in partition1.keys():
            if com1_id not in n_shared_nodes_by_com:
                n_shared_nodes_by_com[com1_id] = 0

        matching = {'matching_results': matching_result, 'n_shared_nodes': n_shared_nodes_by_com}

        return matching
