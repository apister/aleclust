import numpy as np

from pyroaring import BitMap


def generate_population(n):
    return np.arange(n)

def fake_clustering(pop, m):
    rng = np.random.default_rng()
    rpop = pop.copy()
    rng.shuffle(rpop)
    return [BitMap(cluster) for cluster in np.split(rpop, m)]

def generate_clusterings(population, count):
    clusterings = []
    for _ in range(count):
        clustering = fake_clustering(population, 5)
        clusterings.append(clustering)
    return clusterings

def clustering_paths(clustering, clustering_id):
    return [(cluster, [clustering_id], i) for i, cluster in clustering.items()]

def binary_intersections(paths, clustering, clustering_id):
    results = []
    for i, cluster in clustering.items():
        for path in paths:
            cluster1 = path[0]
            inter = cluster.intersection(cluster1)
            if len(inter) > 1:
                clusterings_ids = list(path[1])
                clusterings_ids.append(clustering_id)
                # result is (intersection, cluserings_ids, clusters_ids)
                results.append(tuple([inter] + [clusterings_ids] + list(path[2:]) + [i]))
    return results

def all_intersections(clusterings):
    results = []
    for clustering_id, clustering in clusterings.items():
        if results:
            results += binary_intersections(results, clustering, clustering_id)
            results += clustering_paths(clustering, clustering_id)
        else:
            results = clustering_paths(clustering, clustering_id)
            
    # transform cluster_ids in list for future transformations
    results = [(consensus[0], consensus[1], list(consensus[2:]))for consensus in results]
    
    return results


def filter_intersections(intersections, threshold=1):
    '''Remove intersections which have a number of agreement inferior to the threshold'''
    intersections = [intersection for intersection in intersections if len(intersection[1]) > threshold]
    return intersections


def remove_subsets(intersections):
    '''Delete subsets of intersections if the amplitude of the consensus is the same or lower'''    
    filtered_intersections = []
    for i, intersection in enumerate(intersections):
        for j, intersection2 in enumerate(intersections):
            if i != j:
#                if intersection[0].issubset(intersection2[0]) and (len(intersection[1]) < len(intersection2[1])):
                if intersection[0].issubset(intersection2[0]) and (len(intersection2[0]) > len(intersection[0]) and len(intersection[1]) <= len(intersection2[1]) or len(intersection[1]) < len(intersection2[1])):
                    break
        else:
            filtered_intersections.append(intersection)
    return filtered_intersections


def remove_duplicates(intersections):
    # tuples are hashables
    intersections_tuples = [(tuple(intersection[0]), tuple(intersection[1]), tuple(intersection[2])) for intersection in intersections]
    
    intersections_no_duplicates = set(intersections_tuples)
    
    return intersections_no_duplicates
    
    


def order_results(path):
        # return -len(path[0]) # just by length
        return (-len(path[1]), -len(path[0]))
    
    
def find_consensus_intersections(clusterings, threshold=1):
    intersections = all_intersections(clusterings)
#    intersections = filter_intersections(intersections, threshold)
#    intersections = delete_repetitions(intersections)
#    
#    intersections.sort(key=order_results)
    
    return intersections      
                
            
        
if __name__ == '__main__':
    # Generate 6 clusterings of 5 clusters
    
    POPULATION = generate_population(15)
    CLUSTERINGS = generate_clusterings(POPULATION, 6)
    
    
    INTERSECTIONS = all_intersections(CLUSTERINGS)
    INTERSECTIONS_f = delete_repetitions(INTERSECTIONS)
    
    
    INTERSECTIONS.sort(key=order_results)
    INTERSECTIONS_f.sort(key=order_results)

