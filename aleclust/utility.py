# -*- coding: utf-8 -*-
"""
Created on Wed Nov  6 14:41:14 2019

@author: Aviz
"""
import random
from collections import Counter

def delete_keys_from_dict(dict_del, lst_keys):
    """
        Delete the keys present in lst_keys from the dictionary.
        Loops recursively over nested dictionaries.
    """
    dict_foo = dict_del.copy()  #Used as iterator to avoid the 'DictionaryHasChanged' error
    for field in dict_foo.keys():
        if field in lst_keys:
            del dict_del[field]
        if type(dict_foo[field]) == dict:
            delete_keys_from_dict(dict_del[field], lst_keys)
    return dict_del

def delete_all_keys_from_dict(dict_del, exception):
    """
    Delete all keys from dict except the ones in exception list
    """
    dict_foo = dict_del.copy()
    for key in dict_foo:
        if key not in exception:
            del dict_del[key]
    return dict_del


def dict_to_dict_of_strings(dict_foo):
    """
    Convert type type in string in a dictionary, for json serializability
    """
    for field,value in dict_foo.items():
        if type(value) is type:
            dict_foo[field] = value.__name__
        if type(value) is dict:
            dict_to_dict_of_strings(value)
    
    return dict_foo


def dict_strings(d):
    for k, v in d.items():
        if isinstance(v, dict):
            dict_strings(v)
        else:
            if type(v) == int:
                v = str(v)
            d.update({k: v})
    return d        


def add_list_to_all_nodes(G, attribute_name, attribute_value=[]):
    """
        add a list as an attribute of all nodes of nx graph
    """
    for n in G.nodes():
        # create a new list for each node to avoid referencing the same object
        value = list(attribute_value)
        G.nodes[n][attribute_name] = value


def get_nodes_by_attribute(G, attribute_key, attribute_value, data=False):
    if data:
        return {n:a for n,a in G.nodes(data=True) if attribute_key in a and a[attribute_key] == attribute_value}
    else:
        return [n for n,a in G.nodes(data=True) if attribute_key in a and a[attribute_key] == attribute_value]

def get_attribute_range(G, attribute_key, keep_none=False):
    """
        Compute set of possible values of a given attribute of G among all nodes (i.e the range of the attribute)
    """
    nodes_view = dict(G.nodes.data(attribute_key))
    all_values = nodes_view.values()
    all_values = set(all_values)
    
    if None in all_values and keep_none == False:
        all_values.remove(None)
    
    return all_values


def initialize_random_communities(G, k=3, community_key="community", entity_type_key = None, entity_type = None):
    """
        Initialize random community structure in graph with k communities
    """
    possible_ids = list(range(k))
    if entity_type != None and entity_type_key != None:
        selected_nodes = [n for n,a in G.nodes(entity_type_key) if a == entity_type]
    else:
        selected_nodes = G.nodes()
        
    for n in selected_nodes:
        G.nodes[n][community_key] = random.choice(possible_ids)
        
        
def get_attribute_distribution(G, attribute_key):
    """
        Compute distribution of a given node attribute inside a networkx Graph
    """
    nodes = G.nodes.data(attribute_key)
    values = dict(nodes).values()
    dist = Counter(values)
    return dist


def nested_list_num_elements(listOfElem):
    ''' Get number of elements in a nested list'''
    count = 0
    # Iterate over the list
    for elem in listOfElem:
        # Check if type of element is list
        if type(elem) == list or type(elem) == set:  
            # Again call this function to get the size of this element
            count += nested_list_num_elements(elem)
        else:
            count += 1    
    return count

def del_nested_list(list_foo):
    """ Transform nested lists of len < 1 in their element in a list """
    new_list = []
    for el in list_foo:
        if type(el) == list and len(el) < 2:
            new_list.append(el[0])
        else:
            new_list.append(el)
    return new_list


    

    




        
        


