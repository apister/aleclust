#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 10 22:15:08 2020

@author: alexis
"""

import aleclust.clustering.bipartiteModularity as bipartiteModularity
import aleclust.clustering.biClustering as biClustering
from aleclust.clustering.cdlib_handler import cdlib_handler
import aleclust.clustering.spectral as spectral

from networkx.algorithms import community

# Return_type : a partition is a ensemble of sets of nodes which union gives all nodes of the graph. A sub-partition is when a node does not necesseraly have a community. A cover is when a node can be part of 2 communities.
UNIPARTITE_CDA = {
    "greedy-modularity": {
        "description": "Greedy modularity maximization begins with each node in its own community and joins the pair of communities that most increases modularity until no such pair exists.",
        "parameters": "None",
        "function": community.greedy_modularity_communities,
        "return_type": "partition"
    },
    "fluid": {
        "description": "The algorithm proceeds as follows. First each of the initial k communities is initialized in a random vertex in the graph. Then the algorithm iterates over all vertices in a random order, updating the community of each vertex based on its own community and the communities of its neighbours. This process is performed several times until convergence. At all times, each community has a total density of 1, which is equally distributed among the vertices it contains. If a vertex changes of community, vertex densities of affected communities are adjusted immediately.",
        "parameters": {
            "k":
                {
                    "description": "number of communities to be found",
                    "default": 3,
                    "type": int
                },
            "max_iter":
                {
                    "description": "maximum number of iteration.",
                    "default": 15,
                    "type": int
                },
            "seed":
                {
                    "description": "indicator of random generation state",
                    "default": 0,
                    "type": int
                }
        },
        "function": community.asyn_fluidc,
        "constraints": "Requires a connected graph.",
        "return_type": "partition"
    },
    "percolation": {
        "description": "Find k-clique communities in graph using the percolation method. A k-clique community is the union of all cliques of size k that can be reached through adjacent (sharing k-1 nodes) k-cliques.",
        "parameters": {
            "k": {
                "description": "size of smallest clique",
                "default": 5,
                "type": int
            }
        },
        "function": community.k_clique_communities,
        "return_type": "overlapping"
    },
    "label-propagation": {
        "description": "After initializing each node with a unique label, the algorithm repeatedly sets the label of a node to be the label that appears most frequently among that nodes neighbors. The algorithm halts when each node has the label that appears most frequently among its neighbors.",
        "parameters": {
            "weight": {
                "description": "the edge attribute representing the weight of an edge",
                "default": None,
                "type": str
            },
            "seed": {
                "description": "indicator of random generation state",
                "default": 0,
                "type": int
            }
        },
        "function": community.asyn_lpa_communities,
        "return_type": "partition"
    },
    "girvan-newman": {
        "description": "The Girvan–Newman algorithm detects communities by progressively removing edges from the original graph. The algorithm removes the “most valuable” edge, traditionally the edge with the highest betweenness centrality, at each step. As the graph breaks down into pieces, the tightly knit community structure is exposed and the result can be depicted as a dendrogram.",
        "parameters": "None",
        "function": community.girvan_newman,
        "return_type": "partition"
    },
    "infomap": {
        "description": "Infomap is based on ideas of information theory. The algorithm uses the probability flow of random walks on a network as a proxy for information flows in the real system and it decomposes the network into modules by compressing a description of the probability flow.",
        "parameters": "None",
        "function": cdlib_handler("infomap"),
        "return_type": "partition"
    },
    "louvain": {
        "description": "Louvain maximizes a modularity score for each community. The algorithm optimises the modularity in two elementary phases: (1) local moving of nodes; (2) aggregation of the network. In the local moving phase, individual nodes are moved to the community that yields the largest increase in the quality function. In the aggregation phase, an aggregate network is created based on the partition obtained in the local moving phase. Each community in this partition becomes a node in the aggregate network. The two phases are repeated until the quality function cannot be increased further.",
        "parameters": {
            "weight": {
                "description": "the edge attribute representing the weight of an edge",
                "default": "weight",
                "type": str
            },
            "resolution": {
                "description": "Will change the size of the communities",
                "default": 1,
                "type": float
            },
            "randomize": {
                "description": "Will randomize the node evaluation order and the community evaluation order to get different partitions at each call.",
                "default": False,
                "type": bool
            }
        },
        "function": cdlib_handler("louvain"),
        "return_type": "partition"
    },
    # "markov-clustering": {
    #     "description": "The Markov clustering algorithm (MCL) is based on simulation of (stochastic) flow in graphs. The MCL algorithm finds cluster structure in graphs by a mathematical bootstrapping procedure. The process deterministically computes (the probabilities of) random walks through the graph, and uses two operators transforming one set of probabilities into another. It does so using the language of stochastic matrices (also called Markov matrices) which capture the mathematical concept of random walks on a graph. The MCL algorithm simulates random walks within a graph by alternation of two operators called expansion and inflation.",
    #     "parameters": {
    #         "max_loop" : {
    #                 "description": "maximum number of iterations",
    #                 "default": 1000,
    #                 "type": int
    #                 }
    #         },
    #     "function": cdlib_handler("markov_clustering"),
    #     "return_type": "partition"
    #     },
    "walktrap": {
        "description": "walktrap is an approach based on random walks. The general idea is that if you perform random walks on the graph, then the walks are more likely to stay within the same community because there are only a few edges that lead outside a given community. Walktrap runs short random walks and uses the results of these random walks to merge separate communities in a bottom-up manner.",
        "parameters": "None",
        "function": cdlib_handler("walktrap"),
        "return_type": "partition"
    },
    "em": {
        "description": "EM is based on based on a mixture model. The algorithm uses the expectation–maximization algorithm to detect structure in networks.",
        "parameters": {
            "k": {
                "description": "Number of desired communities",
                "default": 3,
                "type": int
            }
        },
        "function": cdlib_handler("em"),
        "return_type": "partition"
    },
    "spectral-clustering": {
        "description": "Graph embedding with eigen values decompositon. Classical clustering is then applied (like k-means).",
        "parameters": {
            "embedding": {
                "description": "embedding technique used",
                "default": 'slepian',
                "type": str
            },
            "clustering_alg": {
                "description": "clustering algorithm used",
                "default": 'kmeans',
                "type": str
            }
        },
        "function": spectral.spectral_clustering,
        "return_type": "partition"
    },
    # "sbm": {
    #     "description": "Efficient Monte Carlo and greedy heuristic for the inference of stochastic block models.",
    #     "parameters": {
    #         "B_min" : {
    #                 "description": "Minimum number of communities that can be found",
    #                 "default": None,
    #                 "type": int
    #                 },
    #         "B_max" : {
    #                 "description": "Maximum number of communities that can be found",
    #                 "default": None,
    #                 "type": int
    #                 },
    #         "deg_corr" : {
    #                 "description": "If true, use the degree corrected version of the SBM",
    #                 "default": True,
    #                 "type": bool
    #                 }
    #     },
    #     "function": cdlib_handler('sbm_dl'),
    #     "return_type": "partition"
    # }
}

# TODO : some parameters may have no default value
# TODO : HLC to fix
BIPARTITE_CDA = {
    "guimera":
        {
            "description": "This algorithm aims to put the people sharing a lot of \"contracts\" into the same community. It does it by maximising the 'bipartite modularity' with a simulated annealing, i.e. random changes occur in the community structure and these changes are kept or removed with a probability depending of the change in the bipartite modularity.",
            "parameters": {
                "target_entity_type": {
                    "description": "The node set in which the clustering occurs",
                    "default": "people",
                    "type": str
                },
                "c": {
                    "description": "Decay rate of the temperature. The solution will converge faster if it is lower.",
                    "default": 0.95,
                    "type": float
                },
                "f": {
                    "description": "Proportion of the number of local and global change opering at each temperature level. There is fN global and fN**2 local change at each temperature level.",
                    "default": 0.01,
                    "type": float
                },
                "itmax": {
                    "description": "Number maximum of iteration between returning the solution",
                    "default": 5,
                    "type": int
                }
            },
            "function": bipartiteModularity.simulated_annealing,
            "return_type": "bipartition"
        },
    #        "hlc": {
    #            "description" : "Group the links hierarchically into communities, and find the cut of the dendogramm which give the higher global link density among communities",
    #            "parameters": "None",
    #            "function": linkClustering.HLC_handler,
    #            "return_type": "bipartition"
    #            },
    "spectral-co-clustering":
        {
            "description": "Spectral clustering of the Laplacian of the bipartite adjacency matrix. The eigen value decomposition of the Laplacian gives a clustering structure of the graph.",
            "parameters": {
                "k": {
                    "description": "size of smallest clique",
                    "default": 5,
                    "type": int
                },
                "seed": {
                    "description": "indicator of random generation state",
                    "default": 0,
                    "type": int
                },
                "entity_type_key": {
                    "description": "attribute key differentiating the node types inside the graph",
                    "default": "entity_type",
                    "type": str
                },
                "source_entity_type": {
                    "description": "entity type on which the clustering operates",
                    "default": "document",
                    "type": str
                },
            },
            "function": biClustering.spectral_co_clustering,
            "return_type": "bipartition"
        }
}

ATTRIBUTE_UNIPARTITE_CDA = {
    "ilouvain": {
        "description": "The I-Louvain algorithm extends the Louvain approach in order to deal only with the scalar attributes of the nodes. It optimizes Newman’s modularity combined with an entropy measure.",
        "parameters": None,
        "function": cdlib_handler("ilouvain"),
        "return_type": "partition"
    },
    "eva": {
        "description": "The Eva algorithm extends the Louvain approach in order to deal with the attributes of the nodes (aka Louvain Extended to Vertex Attributes). It optimizes - combining them linearly - two quality functions, a structural and a clustering one, namely Newman’s modularity and purity, estimated as the product of the frequencies of the most frequent labels carried by the nodes within the communities. A parameter alpha tunes the importance of the two functions: an high value of alpha favors the clustering criterion instead of the structural one.",
        "parameters": {
            "labels": {
                "description": "dictionary specifying for each node (key) a dict (value) specifying the name attribute (key) and its value (value)",
                "default": None,
                "type": list
            },
            "weight": {
                "description": "optional the key in graph to use as weight.",
                "default": 'weight',
                "type": str
            },
            "resolution": {
                "description": "Will change the size of the communities",
                "default": 1,
                "type": int
            },
            "randomize": {
                "description": "Will randomize the node evaluation order and the community evaluation order to get different partitions at each call,",
                "default": False,
                "type": bool
            },
            "alpha": {
                "description": "Will tune the importance of modularity and purity criteria",
                "default": 0.5,
                "type": 1
            }
        },
        "function": cdlib_handler("eva"),
        "return_type": "partition"
    },
}


STATIC_CDA = {**UNIPARTITE_CDA, **BIPARTITE_CDA, **ATTRIBUTE_UNIPARTITE_CDA}

# TODO : split between uni and bipartite parameters maybe in the future (or with constraints like "bipartite and dynamic" : ...)
PARAMETERS = {
    "projection": {
        "description": "If true, the bipartite graph is projected onto the people set space, giving a unipartite graph",
        "default": True,
        "type": bool
    },
    "projection_set": {
        "description": "entity type on which the projection occurs",
        "default": "people",
        "type": str
    },
    # "timestamped_entity_type": {
    #         "description": "For dynamic community detecion on bipartite graph only. Which entity type set contains the time information.",
    #         "default": "publication",
    #         "type": str
    #         },
    "metrics": {
        "description": "List of metrics to compute on the found communities",
        "default": None,
        "type": list
    },
    # "bimodularity_entity_type": {
    #         "description": "entity type on which the bimodularity is computed (the bimodularity is a metric related to only one entity type, even if the community detection is opering on the 2 sets of the bipartite graph). ",
    #         "default": "people",
    #         "type": str
    #         },
    # "source_entity_type": {
    #         "description": "entity type of the source node set",
    #         "default": "people",
    #         "type": str
    #         },
    # "target_entity_type": {
    #         "description": "entity type of the target node set",
    #         "default": "publication",
    #         "type": str
    #         }
}

METRICS_PARAMETERS = ["bimodularity_entity_type"]


import dcd.TemporalTradeOff.louvain as dynamic_louvain
import dcd.TemporalTradeOff.tnetwork as tnet

DYNAMIC_CDA = {
    "louvain_tdf": {
        "description": "Launch the Louvain algorithm at each snapshot, with an initial condition corresponding to the result of the previous timeslot. Use an heuristic to not find a local optimum too fast",
        "parameters": {
            "resolution": {
                "description": "Will change the size of the communities",
                "default": 1,
                "type": float
            },
            "randomize": {
                "description": "Will randomize the node evaluation order and the community evaluation order to get different partitions at each call",
                "default": None,
                "type": bool
            },
            "weight": {
                "description": "Optional. The key in the graph to use as weight.",
                "default": 'weight',
                "type": str
            },
        },
        "function": dynamic_louvain.Louvain,
        "return_type": "dynamic_partition"
    },
    "graph_smooth": {
        "description": "This approach is a naive implementation of the idea proposed in [1]. To sum up, at each snapshot, a new graph is create which is the combination of the graph at this step and a graph in which edges are present between any two nodes belonging to the same community in the previous step.",
        "parameters": {
            "alpha": {
                "description": "parameter setting relative importance of past VS current graph. 1: only current, 0: only previous",
                "default": 0.9,
                "type": float
            },
        },
        "function": tnet.SmoothGraph,
        "return_type": "dynamic_partition"
    }
}
