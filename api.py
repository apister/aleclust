# -*- coding: utf-8 -*-

import os
import rapidjson
import copy
import sys
import time

from flask import Flask, jsonify, request, send_file
from flask_socketio import SocketIO, emit, send, Namespace
from flask_cors import CORS
from flask import current_app
import requests
from urllib.parse import urlparse
from io import BytesIO
from networkx.readwrite import json_graph

from dcd.InstantOptimal import InstantOptimal

import aleclust.processing.preprocessing as preprocessing
import aleclust.utility as utility
import aleclust.dynamiClustering.dynamiClustering as dynamiClustering
import aleclust.ensemble.pk_clustering as ensemble
import aleclust.ensemble.ensembleGraph as ensembleGraph
import aleclust.clustering.clustering as clustering
from aleclust.processing.paohJsonParser import PaohJsonParser
from aleclust.processing.parameters import Parameters

from aleclust.globals import STATIC_CDA, BIPARTITE_CDA, UNIPARTITE_CDA, ATTRIBUTE_UNIPARTITE_CDA, PARAMETERS, \
    METRICS_PARAMETERS, DYNAMIC_CDA

INFO = "aleclust is an API for community detection in graphs. It takes as input a graph in networkx json format, " \
       "and return the nodes in json format with a community id. Several algorithm of community detection are " \
       "implemented. Some algorithms work for unipartite graph and other for bipartites. You can use aleclust with " \
       "GET requests, using one of the available datasets in /clustering/[DATASET]/[ALGORITHM], or with a POST " \
       "request posting your own dataset in /clustering/[ALGORITHM]. You can modifiy the parameters of the algorithm " \
       "directly in the URL like this : /clustering/ex_3TS/fluid?k=5&seed=1. Several global parameters can be " \
       "modified in the same fashion. These are described in the PARAMETERS field. Dynamic community detection work " \
       "almost as the the same. You just have to choose the path /dynamiClustering/[DATASET]/[ALGORITHM] for GET " \
       "request and /dynamiClustering/[ALGORITHM] for POST requests. Each node will be given list of communities, " \
       "one for each timeslot. The same algorithms than in static clustering can be used."

app = Flask(__name__)
CORS(app)

root_path = app.root_path
dataPaths = {
    "IEEE_VIS_4y": root_path + "/data/new_json/IEEE_VIS_2012_2016_InfoVis.json",
    "buenosAires": root_path + "/data/buenosAires/buenosAires_11-12-19.json",
    "eurovis_15y": root_path + "/data/new_json/EuroVis_Data_2000_2015.json",
    "eurovis_2y": root_path + "/data/new_json/EuroVis_Data_2004_2006.json",
    "test_v2": root_path + "/data/new_json/test.json"
}

@app.route("/", methods=["GET"])
def base():
    message = {
        "info": INFO,
        "datasets": list(dataPaths.keys()),
        "algorithms": {"unipartite": utility.dict_to_dict_of_strings(
           utility.delete_keys_from_dict(copy.deepcopy(UNIPARTITE_CDA), ["function"])),
           "bipartite": utility.dict_to_dict_of_strings(
               utility.delete_keys_from_dict(copy.deepcopy(BIPARTITE_CDA), ["function"]))},
        "parameters": utility.dict_to_dict_of_strings(PARAMETERS),
        "metrics": list(clustering.METRICS.keys())
    }
    return jsonify(message)


# TODO : redo this
@app.route("/data/<string:dataName>", methods=["GET"])
def getDataset(dataName):
    if os.path.splitext(dataPaths[dataName])[1] == ".json":
        G = preprocessing.loadGraphfromJson(dataPaths[dataName])
    else:
        G = clustering.importEdgelist(dataPaths[dataName])
    Gjson = json_graph.node_link_data(G)
    return jsonify(Gjson)


def set_parameters_default(clusteringAlgorithm, clustering_parameters, preprocessing_parameters):
    # get default values for clustering parameters not found in the request
    if STATIC_CDA[clusteringAlgorithm]["parameters"] != None and STATIC_CDA[clusteringAlgorithm][
        "parameters"] != "None":
        for parameter in STATIC_CDA[clusteringAlgorithm]["parameters"]:
            if parameter not in clustering_parameters.keys():
                clustering_parameters[parameter] = STATIC_CDA[clusteringAlgorithm]["parameters"][parameter][
                    "default"]

    # get default values for needed preprocessing parameters not found in the request
    for parameter in PARAMETERS:
        if parameter not in preprocessing_parameters.keys():
            preprocessing_parameters[parameter] = PARAMETERS[parameter]["default"]


def static_clustering_request(flask_request, clustering_algo, dataName=False, dynamic=False):
    json = flask_request.json

    json_parser = PaohJsonParser(json)
    parameters: Parameters = json_parser.parameters

    # get URL arguments
    args = flask_request.args
    parameters.parse_parameters(clustering_algo, args)

    graph = json_parser.own_json_to_graph()

    results = clustering.doClustering(graph, clustering_algo, metrics_to_compute=parameters.metrics,
                                          metrics_args={}, **parameters.clustering_parameters)
    return results


def dynamic_clustering_request(flask_request, clustering_algo):
    from dcd import json_read
    json = flask_request.json

    if clustering_algo in DYNAMIC_CDA:
        json_parser = PaohJsonParser(json, dynamic=True)
        parameters: Parameters = json_parser.parameters
        parameters.algorithm_list = DYNAMIC_CDA

        # get URL arguments
        args = flask_request.args
        parameters.parse_parameters(clustering_algo, args)
        dyn_graph = json_parser.own_json_to_graph(dynamic=True)

        model = DYNAMIC_CDA[clustering_algo]["function"](**parameters.clustering_parameters)
    else:
        dyn_graph = PaohJsonParser.json_to_dyn_graph(json, True)
        model = InstantOptimal(clustering_algo)

    dyn_coms = model.apply(dyn_graph)
    dyn_coms_json = dyn_coms.to_json()

    return dyn_coms_json


@app.route("/clustering/<string:dataName>/<clustering_algo>", methods=["GET"])
def get_clusters(dataName, clustering_algo):
    nodes_info = static_clustering_request(request, clustering_algo, dataName=dataName, dynamic=False,
                                           request_type="GET")
    return jsonify(nodes_info)


@app.route("/clustering/<string:clustering_algo>", methods=["POST"])
def ask_clusters(clustering_algo):
    nodes_info = static_clustering_request(request, clustering_algo, dataName=None, dynamic=False)
    return jsonify(nodes_info)


@app.route("/dynamiClustering/<clustering_algo>", methods=["POST"])
def ask_dynamic_clusters(clustering_algo):
    nodes_info = dynamic_clustering_request(request, clustering_algo)
    return jsonify(nodes_info)


@app.route("/prior", methods=["POST"])
def ask_ensemble():
    import numpy as np
    import random as rn

    rn.seed(0)
    np.random.seed(0)

    json = request.json
    # print("DATA ", json)

    # get URL arguments
    args = request.args
    args_dict = Parameters.args_to_dict(args)

    if 'attributes' in args_dict.keys():
        attributes = args_dict['attributes']
        if attributes == 'None':
            attributes = None
    else:
        attributes_metadata = json['metadata']['community']
        attributes = []
        if "Prior Knowledge" in attributes:
            attributes.remove("Prior Knowledge")

        # TODO: find attributes
        # for attr in attributes_metadata:
        #     if attr == "Prior Knowledge":
        #         continue
        #
        #     first_value_attribute = json["nodes"][0][attr]
        #     try:
        #         x = float(first_value_attribute)
        #         x = int(first_value_attribute)
        #         attribute = attr + "_int"
        #     except Exception:
        #         attribute = attr + "_string"
        #     attributes.append(attribute)

        attributes.append('time_int')

    if 'algorithms' in args_dict.keys():
        algorithms = args_dict['algorithms']
    else:
        algorithms = ["guimera", "greedy-modularity", "spectral-co-clustering", "girvan-newman", "louvain", "fluid",
                      "label-propagation", "walktrap", "em", "infomap"]
        # I deleted girvan-newman for now because it was long on the MB dataset
        # algorithms = ["guimera", "greedy-modularity", "spectral-co-clustering", "louvain", "fluid", "label-propagation"] # 'markov-clustering']
        # same for label propagation
    #        algorithms = ["guimera", "greedy-modularity", "spectral-co-clustering", "louvain", "fluid"]
    #        algorithms =  ["guimera", "greedy-modularity", "louvain", "fluid", "label-propagation"]

    start_time = time.time()

    ensemble_cd = ensemble.PkClustering(json, algorithms)
    run_semi_supervised = False if ensemble_cd.n_pk == 0 else True
    ensemble_cd.run(attributes, run_semi_supervised, grid_search=True)

    if ensemble_cd.n_pk == 0:
        ensemble_graph = ensembleGraph.EnsembleGraph(ensemble_cd)
        ensemble_graph.ensemble_clustering()
        ensemble_graph.match_algorithms_with_ensemble()
        ensemble_cd.merge_results()
        ensemble_graph.add_ensemble_to_other_clusterings()
    else:
        ensemble_cd.match_all_algorithms()
        ensemble_cd.merge_results()

    print('TIME TAKEN : ', time.time() - start_time)
    current_app.ensemble = ensemble_cd

    # print('OUTPUT: ', ensemble_cd.all_clusterings_json)
    return jsonify(ensemble_cd.all_clusterings_json)


@app.route("/oneAlgorithmIntersections", methods=["GET"])
def ask_intersections():
    args = request.args
    args_dict = Parameters.args_to_dict(args)

    if 'algorithmRef' not in args_dict.keys():
        return 'algorithmRef parameter is required'
    else:
        algorithm = args_dict['algorithmRef']

    if 'algorithm_list' not in args_dict.keys():
        return 'algorithm_list parameter is required'
    else:
        algorithm_list = args_dict['algorithm_list']

    result = current_app.ensemble.all_matching_from_algorithm(algorithm, algorithm_list)

    # print("consenus ", result)

    return jsonify(result)


# Allow url in paohvis
@app.route("/getFile", methods=["POST"])
def sendFileFromUrl():
    try:
        url = request.args.get('url', '')
        extension = os.path.splitext(urlparse(url).path)[-1]

        initial_timeout = 1
        maximum_file_size = 1000000

        if extension in ['.csv', '.pao', '.json']:
            filename = os.path.basename(urlparse(url).path)
            with requests.get(url, verify=False, stream=True,
                              timeout=initial_timeout) as response:  # TODO: change verify false
                response.raise_for_status()

                if int(response.headers.get('Content-Length')) > maximum_file_size:
                    raise ValueError('response too large')

                response = send_file(BytesIO(response.content), as_attachment=True, attachment_filename=filename)
                response.headers["x-filename"] = filename
                response.headers["Access-Control-Expose-Headers"] = 'x-filename'
                return response

    except (RuntimeError, TypeError, NameError, ValueError):
        return jsonify("error"), 404

    return jsonify("error"), 404


debug = True
if __name__ == "__main__":
    if len(sys.argv) > 1:
        app.run(debug=debug, port=sys.argv[1])
    else:
        #        socketio.run(app, port=10080)
        app.run(debug=debug, port=10085)
        # app.run(debug=debug, port=10085)
