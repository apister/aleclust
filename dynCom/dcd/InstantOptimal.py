import networkx as nx
import dynetx as dn
from cdlib import algorithms
from cdlib import TemporalClustering
from networkx.generators.community import LFR_benchmark_graph

from dcd import utils, dynCommunities


def cdlib_handler(algorithm_name):
    cdlib_function = algorithms.__dict__[algorithm_name]
    return cdlib_function


def t_com_id_to_com_id(t_com_id):
    return t_com_id.split("_")[-1]


def jaccard(x, y):
    return len(set(x) & set(y)) / len(set(x) | set(y))


class InstantOptimal:
    def __init__(self, alg_name):
        self.alg_name = alg_name

        self.dyn_communities = None
        self.temporal_clustering = None
        self.snapshot_ids = None
        self.matches = None
        self.tcomids_to_comid = None
        self.com_id = None

    def apply(self, dyn_graph, **kwargs):
        self.dyn_communities = dynCommunities.DynCommunities(dyn_graph)
        self.temporal_clustering = TemporalClustering()
        self.snapshot_ids = dyn_graph.temporal_snapshots_ids()

        for i, snapshot_id in enumerate(self.snapshot_ids):
            snapshot = utils.dyn_to_nxgraph(dyn_graph.time_slice(snapshot_id))
            partition = cdlib_handler(self.alg_name)(snapshot, **kwargs)
            self.temporal_clustering.add_clustering(partition, snapshot_id)

        self.matches = self.temporal_clustering.community_matching(jaccard, two_sided=False)
        self.matching_step()

        self.save_to_dyn_communities()
        return self.dyn_communities

    def matching_step(self):
        self.tcomids_to_comid = {} # id of snapshot com ids to ids of global communities
        self.com_id = 0
        for match in self.matches:
            similarity = match[2]
            com_id_t0 = match[0]
            com_id_t1 = match[1]

            if com_id_t0 in self.tcomids_to_comid:
                com_id = self.tcomids_to_comid[com_id_t0]
            else:
                com_id = self.com_id
                self.tcomids_to_comid[com_id_t0] = com_id
                self.com_id += 1

            if similarity > 0.5:
                self.tcomids_to_comid[com_id_t1] = com_id

    def save_to_dyn_communities(self):
        for snapshot_id in self.snapshot_ids:
            for j, com in enumerate(self.temporal_clustering.get_clustering_at(snapshot_id).communities):
                t_com_id = f"{snapshot_id}_{j}"
                if t_com_id in self.tcomids_to_comid:
                    com_id = self.tcomids_to_comid[f"{snapshot_id}_{j}"]
                else:
                    com_id = self.com_id
                    self.com_id += 1

                self.dyn_communities.communities[snapshot_id][com_id] = com

        # print(self.dyn_communities.communities)
