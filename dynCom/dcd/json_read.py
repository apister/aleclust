import rapidjson
import dynetx as dn


def paoh_json_reader(json_data):
    # graph_dyn = DynNetwork()
    graph_dyn = dn.DynGraph()

    metadata = json_data["metadata"]
    source_entity_type = metadata["source_entity_type"]
    target_entity_type = metadata["target_entity_type"]

    graph_dyn.graph["source_nodes"] = []
    graph_dyn.graph["target_nodes"] = []

    for node in json_data["nodes"]:
        graph_dyn.add_node(node["id"])
        if node["entity_type"] == source_entity_type:
            graph_dyn.graph["source_nodes"].append(node["id"])
        elif node["entity_type"] == target_entity_type:
            graph_dyn.graph["target_nodes"].append(node["id"])

    edges_time_sorted = sorted(json_data["links"], key=lambda link: int(node["ts"]))

    for edge in edges_time_sorted:
        graph_dyn.add_interaction(edge["source"], edge["target"], t=int(edge["ts"]))

    return graph_dyn
