from abc import abstractmethod

import networkx as nx
import dynetx as dn

from dcd import utils, dynCommunities


class GlobalOptUpdate:
    def __init__(self):
        self.partitions = []
        self.dyn_communities = None
        self.snapshot_ids = None

    @abstractmethod
    def snapshot_clustering(self, snapshot: nx.Graph, init_partition=None):
        return

    @abstractmethod
    def convert_partition(self):
        return

    def apply(self, dyn_graph: dn.DynGraph):
        self.dyn_communities = dynCommunities.DynCommunities(dyn_graph)
        self.snapshot_ids = dyn_graph.temporal_snapshots_ids()

        for i, snapshot_id in enumerate(self.snapshot_ids):
            # print(i, snapshot_id)
            snapshot = utils.dyn_to_nxgraph(dyn_graph.time_slice(snapshot_id))

            if i == 0:
                partition = self.snapshot_clustering(snapshot, None)
            else:
                partition_init = self.process_previous_partition(self.partitions[i - 1], snapshot.nodes())
                partition = self.snapshot_clustering(snapshot, partition_init)

            self.partitions.append(partition)

        self.save_to_dyn_communities()
        return self.dyn_communities

    @staticmethod
    def process_previous_partition(partition_t0, nodes_t1):
        partition_t1_init = {}
        for node in nodes_t1:
            if node in partition_t0:
                partition_t1_init[node] = partition_t0[node]
            else:
                partition_t1_init[node] = None
        return partition_t1_init

    def save_to_dyn_communities(self):
        for partition, snapshot_id in zip(self.partitions, self.snapshot_ids):
            self.dyn_communities.communities[snapshot_id] = self.convert_partition(partition)
