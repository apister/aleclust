import community as community_louvain

from dcd import utils
from dcd.TemporalTradeOff import GlobalOptUpdate


class Louvain(GlobalOptUpdate.GlobalOptUpdate):
    def __init__(self, resolution=1.0, randomize=None, weight="weight"):
        GlobalOptUpdate.GlobalOptUpdate.__init__(self)

        self.resolution = resolution
        self.randomize = randomize
        self.weight = weight

    def snapshot_clustering(self, snapshot, init_partition):
        snapshot_partition = community_louvain.best_partition(snapshot, partition=init_partition,
                                                              resolution=self.resolution, randomize=self.randomize)
        return snapshot_partition

    def convert_partition(self, partition):
        return utils.nodes_to_coms_to_coms_to_nodes(partition)
