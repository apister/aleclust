from abc import abstractmethod

import networkx as nx
import dynetx as dn
from tnetwork.DCD import rollingCPM, smoothed_graph

from dcd import utils, dynCommunities

class RollingCPM:
    def __init__(self):
        pass

    def apply(self, dyn_graph: dn.DynGraph):
        tn = utils.dyngraph_to_dyngraphSN(dyn_graph)
        coms = rollingCPM(tn)
        print(coms)
        print(coms.affiliations(2002))
        print(coms.communities(2002))
        print(coms.communities_duration())
        print(coms.affiliations_durations())
        print(coms.snapshots_timesteps())


class SmoothGraph:
    def __init__(self, alpha=0.9):
        self.alpha = alpha

    def apply(self, dyn_graph: dn.DynGraph):
        tn = utils.dyngraph_to_dyngraphSN(dyn_graph)
        coms = smoothed_graph(tn, alpha=self.alpha)
        dyn_coms = utils.dynCommuntiesSN_to_dynCommunities(coms, dyn_graph)
        return dyn_coms

