from dcd.dynNetwork import DynNetwork
import dynetx as dn
import itertools
import rapidjson

from dcd import json_read


def projection(graph_dyn: DynNetwork):
    graph_dyn_uni = dn.DynGraph()
    snapshot_ids = graph_dyn.temporal_snapshots_ids()

    for snapshot_id in snapshot_ids:
        snapshot = graph_dyn.time_slice(snapshot_id)
        snapshot_nodes = snapshot.nodes()

        for target_node in graph_dyn.graph["target_nodes"]:
            graph_dyn_uni.add_node(target_node)

        for source_node in graph_dyn.graph["source_nodes"]:
            if source_node in snapshot_nodes:
                target_nodes = snapshot.neighbors(source_node)
                target_pairs = list(itertools.combinations(target_nodes, 2))
                graph_dyn_uni.add_interactions_from(target_pairs, snapshot_id)

        for target_node in graph_dyn.graph["target_nodes"]:
            if target_node not in graph_dyn_uni.nodes():
                graph_dyn_uni.add_node(target_node)

    print("projection finished")
    return graph_dyn_uni
