from collections import defaultdict


def dict_to_list_communities(dict_communities):
    list_com = []
    for node_id, communities in dict_communities.items():
        list_com.append({"id": node_id, "communities": communities})

    return list_com


class DynCommunities:
    def __init__(self, dyn_graph=None):
        self.dyn_graph = dyn_graph
        # self.communities: dict[str, dict[int, list]] = {}  # Time to com to node
        self.communities: dict[str, dict[int, list]] = defaultdict(lambda : defaultdict(list)) # Time to com to node
        self.node_to_communities = defaultdict(list)
        self.json = None

        self.no_community_label = None

    def compute_node_to_communities(self):
        self.node_to_communities = defaultdict(list)
        for snapshot_id, partition in self.communities.items():
            labeled_nodes = []
            for com_id, nodes in partition.items():
                labeled_nodes = labeled_nodes + nodes
                for node_id in nodes:
                    self.node_to_communities[node_id].append(com_id)

            for node_id in self.dyn_graph.nodes():
                if node_id not in labeled_nodes:
                    self.node_to_communities[node_id].append(self.no_community_label)

    def smooth(self):
        for node, communities in self.node_to_communities.items():
            indexes_to_replace = []

            see_null = False
            last_value = communities[0]
            for i, static_com in enumerate(communities):
                if static_com == self.no_community_label:
                    indexes_to_replace.append(i)
                    see_null = True
                    continue

                if see_null:
                    if static_com == last_value:
                        for j in indexes_to_replace:
                            communities[j] = static_com
                    indexes_to_replace = []

                see_null = False
                last_value = static_com


    def to_json(self, smooth=True):
        if not self.node_to_communities:
            self.compute_node_to_communities()

        if smooth: self.smooth()
        self.json = {}
        # nodes_communities = defaultdict(list)
        # for snapshot_id, partition in self.communities.items():
        #     labeled_nodes = []
        #     for com_id, nodes in partition.items():
        #         labeled_nodes = labeled_nodes + nodes
        #         for node_id in nodes:
        #             nodes_communities[node_id].append(com_id)
        #
        #     for node_id in self.dyn_graph.nodes():
        #         if node_id not in labeled_nodes:
        #             nodes_communities[node_id].append(self.no_community_label)

        self.json["nodes"] = dict_to_list_communities(self.node_to_communities)
        self.json["graph"] = {}

        return self.json
