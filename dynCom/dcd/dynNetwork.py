from dynetx import DynGraph

# Error with the inheritance
class DynNetwork(DynGraph):
    def __init__(self):
        DynGraph.__init__(self)
        self.source_nodes = []
        self.target_nodes = []

        self.nodeAttributes = {}
        self.edges_weight = {}
