import copy
from collections import defaultdict

import networkx as nx
import tnetwork

import dcd.utils as utils
from dcd.dynCommunities import DynCommunities


def dyn_to_nxgraph(dyn_graph):
    graph = copy.deepcopy(dyn_graph)
    graph.__class__ = nx.Graph

    return graph


def nodes_to_coms_to_coms_to_nodes(nodes_to_coms):
    coms_to_nodes = defaultdict(list)
    for node, com_id in nodes_to_coms.items():
        coms_to_nodes[com_id].append(node)
    return dict(coms_to_nodes)

#TODO : Not finished
def temporal_clustering_to_dyn_communities(dyn_graph, temporal_clustering):
    dyn_coms = DynCommunities(dyn_graph)
    # matching = temporal_clustering.get_explicit_community_match()

    # print("match ", matching)
    print("match ", temporal_clustering.matching)

    snapshot_comid_to_comid = {}
    comid_count = 1

    for snapshot_id in temporal_clustering.get_observation_ids():
        for j, com in enumerate(temporal_clustering.get_clustering_at(snapshot_id).communities):
            s_com_id = f"{snapshot_id}_{j}"
            if s_com_id in snapshot_comid_to_comid:
                com_id = snapshot_comid_to_comid[s_com_id]
            else:
                com_id = comid_count
                comid_count += 1

            dyn_coms.communities[snapshot_id][com_id] = com


def dyngraph_to_dyngraphSN(dyn_graph):
    tn = tnetwork.DynGraphSN()

    snapshot_ids = dyn_graph.temporal_snapshots_ids()
    for i, snapshot_id in enumerate(snapshot_ids):
        snapshot = utils.dyn_to_nxgraph(dyn_graph.time_slice(snapshot_id))
        tn.add_snapshot(snapshot_id, snapshot)

    return tn

def dynCommuntiesSN_to_dynCommunities(dyn_communitiesSN, dyn_graph):
    dyn_communities = DynCommunities(dyn_graph)

    for timeslot in dyn_communitiesSN.snapshots_timesteps():
        for node_id, coms in dyn_communitiesSN.affiliations(timeslot).items():
            dyn_communities.communities[timeslot][list(coms)[0]].append(node_id)

    return dyn_communities





