import dynetx

from dcd import projection


def test_projection(graph_dyn_bipartite):
    graph_dyn_uni = projection.projection(graph_dyn_bipartite)

    assert type(graph_dyn_uni) == dynetx.DynGraph
    assert len(graph_dyn_uni) == len(graph_dyn_bipartite.graph["target_nodes"])
