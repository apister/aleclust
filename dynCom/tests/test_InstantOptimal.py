from dcd import InstantOptimal

def test_apply(marie_boucher_graph):
    model = InstantOptimal.InstantOptimal("louvain")
    dyn_communities = model.apply(marie_boucher_graph)
    print(dyn_communities.to_json())

    model = InstantOptimal.InstantOptimal("infomap")
    dyn_communities = model.apply(marie_boucher_graph)
    print(dyn_communities.to_json())
