import networkx as nx
import cdlib.algorithms as algorithms

from dcd.utils import dyn_to_nxgraph, temporal_clustering_to_dyn_communities, dyngraph_to_dyngraphSN


def test_dyn_to_nxgraph(graphs_dyn):
    graph = dyn_to_nxgraph(graphs_dyn)

    assert type(graph) == nx.Graph
    assert set(graphs_dyn.nodes()) == set(graph.nodes())
    assert {(u, v) for u, v, t in graphs_dyn.interactions()} == set(graph.edges())


def test_temporal_clustering_to_dyn_communities(marie_boucher_graph, inria_graph):
    temporal_coms = algorithms.tiles(marie_boucher_graph, 1)
    dynamic_coms = temporal_clustering_to_dyn_communities(marie_boucher_graph, temporal_coms)

    temporal_coms = algorithms.tiles(inria_graph, 1)
    dynamic_coms = temporal_clustering_to_dyn_communities(marie_boucher_graph, temporal_coms)


def test_dyngraph_to_dyngraph_sn(graphs_dyn):
    tn = dyngraph_to_dyngraphSN(graphs_dyn)
    for interaction in graphs_dyn.interactions():
        time_interval = interaction[2]['t'][0]
        for t in time_interval:
            edge = (frozenset([interaction[0], interaction[1]]), t)
            assert edge in tn.interactions()

