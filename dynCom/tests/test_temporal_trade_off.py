from TemporalTradeOff import louvain

import dcd.TemporalTradeOff.tnetwork as tnetwork_DCD
from dcd.utils import dynCommuntiesSN_to_dynCommunities

class TestGlobalOptUpdate:
    def test_snapshot_clustering(self):
        assert True

    def test_apply(self, graphs_dyn):
        louvain_model = louvain.Louvain()
        dyn_communities = louvain_model.apply(graphs_dyn)

        assert len(dyn_communities.communities) == len(graphs_dyn.temporal_snapshots_ids())

        for snapshot_id in graphs_dyn.temporal_snapshots_ids():
            print(dyn_communities.communities[snapshot_id])
            snapshot = graphs_dyn.time_slice(snapshot_id)
            assert len(snapshot) == len([node for com_id, com in dyn_communities.communities[snapshot_id].items() for
                                         node
                                         in com])

        print(dyn_communities.to_json())

    def test_save_to_dyn_communities(self):
        assert True


def test_rollingCPM(inria_graph):
    rollingCPM = tnetwork_DCD.RollingCPM()
    coms = rollingCPM.apply(inria_graph)

def test_smoothgraph(marie_boucher_graph):
    smooth_graph = tnetwork_DCD.SmoothGraph()
    coms = smooth_graph.apply(marie_boucher_graph)

def test_smoothgraph_2(inria_graph):
    smooth_graph = tnetwork_DCD.SmoothGraph()
    coms = smooth_graph.apply(inria_graph)
