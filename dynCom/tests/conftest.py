import pathlib

import pytest
import rapidjson
import dynetx as dn
import networkx as nx

from dcd import json_read, projection

this_file_path = str(pathlib.Path(__file__).parent.absolute())
data_path = this_file_path + "/../data/"
marie_boucher_small_fp = data_path + "Marie_Boucher_small.json"
marie_boucher_fp = data_path + "Marie_Boucher.json"
inria_fp = data_path + "inria-collab-g.json"


@pytest.fixture(scope="session")
def graph_dyn_bipartite():
    json_data = rapidjson.loads(open(marie_boucher_small_fp, "r").read())
    G_bipartite = json_read.paoh_json_reader(json_data)
    return G_bipartite


@pytest.fixture(scope="session", params=[marie_boucher_small_fp, "generate"])
def graphs_dyn(request):
    if request.param == "generate":
        G = dn.DynGraph(edge_removal=True)
        G.add_interactions_from([(1, 2), (2, 3), (1, 10)], t=10)
        G.add_interactions_from([(1, 2), (2, 3), (3, 4)], t=11)
        G.add_interactions_from([(1, 2), (2, 3), (1, 3)], t=12)
        G.add_interaction(1, 3, t=13)
    else:
        json_data = rapidjson.loads(open(request.param, "r").read())
        G_bipartite = json_read.paoh_json_reader(json_data)
        G = projection.projection(G_bipartite)

    return G

@pytest.fixture(scope="session")
def small_graph():
    G = dn.DynGraph(edge_removal=True)
    G.add_interactions_from([(1, 2), (2, 3), (1, 10)], t=10)
    G.add_interactions_from([(1, 2), (2, 3), (3, 4)], t=11)
    G.add_interactions_from([(1, 2), (2, 3), (1, 3)], t=12)
    G.add_interaction(1, 3, t=13)
    return G

@pytest.fixture(scope="session")
def marie_boucher_small_graph():
    json_data = rapidjson.loads(open(marie_boucher_small_fp, "r").read())
    G_bipartite = json_read.paoh_json_reader(json_data)
    G = projection.projection(G_bipartite)
    return G

@pytest.fixture(scope="session")
def marie_boucher_graph():
    json_data = rapidjson.loads(open(marie_boucher_fp, "r").read())
    G_bipartite = json_read.paoh_json_reader(json_data)
    G = projection.projection(G_bipartite)
    return G

@pytest.fixture(scope="session")
def inria_graph():
    json_data = rapidjson.loads(open(inria_fp, "r").read())
    G_bipartite = json_read.paoh_json_reader(json_data)
    G = projection.projection(G_bipartite)
    return G

