from dcd.dynCommunities import DynCommunities


def test_smooth(small_graph):
    dyn_coms = DynCommunities(small_graph)
    dyn_coms.communities = {
        0: {0: [1, 2], 1: [3, 10]},
        1: {0: [1, 2]},
        2: {0: [], 1: [4, 10, 3]},
        3: {2: [1, 2, 3]}
    }
    dyn_coms.compute_node_to_communities()
    # print(dyn_coms.node_to_communities)
    dyn_coms.smooth()
    # print(dyn_coms.node_to_communities)

    assert dyn_coms.node_to_communities[3] == [1, 1, 1, 2]
