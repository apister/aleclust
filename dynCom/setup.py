"""
Setup file for dynCom
"""

from setuptools import setup

PACKAGES = [
    'dcd',
    'dcd.TemporalTradeOff'
]


def read(fname):
    "Return the content of fname as a string"
    with open(fname) as infile:
        return infile.read()


setup(
    name="dynCom",
    version="0.0.3",
    author="Alexis Pister",
    author_email="alexis.pister@inria.fr",
    description="Dynamic Graph Clustering Library",
    license="BSD",
    keywords="dynamic network, clustering",
    packages=PACKAGES,
    long_description=read('README.md'),
    classifiers=["Development Status :: 2 - PRe-Alpha",
                 "Topic :: Scientific/Engineering :: Visualization",
                 "Topic :: Scientific/Engineering :: Information Analysis",
                 "License :: OSI Approved :: BSD License"],
    platforms='any',
    python_requires='>=3.6',
    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    # install_requires=required,
    install_requires=[
        "networkx>=2.5",
        "dynetx>=0.2.3",
        "cdlib>=0.2.3",
        "tnetwork"
    ],
    package_data={
        # If any package contains *.md, *.txt or *.rst files, include them:
        'doc': ['*.md', '*.rst'],
    }
)
