"""
Setup file for aleClust.
"""

from setuptools import setup

PACKAGES = [
    'aleclust',
    'aleclust.processing',
    'aleclust.ensemble',
    'aleclust.clustering',
]


def read(fname):
    "Return the content of fname as a string"
    with open(fname) as infile:
        return infile.read()


setup(
    name="aleClust",
    version="0.0.1a1",
    author="Alexis Pister",
    author_email="alexis.pister@inria.fr",
    url="hhttps://gitlab.inria.fr/apister/aleclust",
    description="Alexis Clustering Library",
    license="BSD",
    keywords="network, clustering",
    packages=PACKAGES,
    long_description=read('README.md'),
    classifiers=["Development Status :: 2 - PRe-Alpha",
                 "Topic :: Scientific/Engineering :: Visualization",
                 "Topic :: Scientific/Engineering :: Information Analysis",
                 "License :: OSI Approved :: BSD License"],
    platforms='any',
    python_requires='>=3.6',
    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    # install_requires=required,
    install_requires=[
        "python-louvain",
        "cdlib>=0.2.3",
        "docutils==0.15.2",
        "pquality==0.0.7",
        "bimlpa",
        "demon>=2.0.4",
        "eva-lcd",
        "fastcache==1.1.0",
        "mock==3.0.5",
        "future==0.17.1",
        "matplotlib",
        "filelock",
        "nf1==0.0.3",
        "omega-index-py3==0.3",
        "filelock",
        "Flask>=1.1.1",
        "Flask-Cors>=3.0.8",
        "Flask-SocketIO>=4.2.1",
        "Jinja2>=2.10.3",
        "karateclub",
        "numpy>=1.11.3",
        "scipy>=0.18.1",
        "pyroaring",
        "py-stringmatching==0.4.1",
        "pulp>=2.1",
        "python-rapidjson==0.9.1",
        "pytiled-parser==0.9.3",
        "networkx",
        "requests>=2.22.0",
        "scikit-learn",
        "pandas",
        "timeout_decorator",
        "torch==1.4.0"],
        # "graphconverter"],
    package_data={
        # If any package contains *.md, *.txt or *.rst files, include them:
        'doc': ['*.md', '*.rst'],
        }
    )
