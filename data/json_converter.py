# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 13:16:02 2019

@author: Aviz
"""
import rapidjson

# Import of graphconverter
import sys
graphconverterPath = "C:\\Users\Aviz\SpyderProjects\graph_converter"
sys.path.insert(1, graphconverterPath)
from graphconverter.io.json_converter import convert_json




data_folder_input = "./paohvis/"
data_folder_output = "./new_json/"

datasets = ["IEEE_VIS_2007_2016.json", "IEEE_VIS_2012_2016_InfoVis.json", "EuroVis_Data_2000_2015.json"]
datasets2 =  ["EuroVis_Data_2004_2006.json"]
datasets3 = ["IEEE_VIS_2007_2016.json"]

for dataset in datasets3:
    print(dataset)
    input_path = data_folder_input + dataset
    output_path = data_folder_output + dataset
    
    json_input = rapidjson.load(open(input_path, "r"))
    
    # Load schema
    schema_path = "new_json/schema.json"
    schema =  rapidjson.load(open(schema_path))
    
    # result
    json_output = convert_json(json_input, schema = schema)
    
    # save to json file
    rapidjson.dump(json_output, stream=open(output_path, "w"))