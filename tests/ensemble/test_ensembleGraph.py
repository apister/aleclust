from aleclust.ensemble.ensembleGraph import EnsembleGraph
from aleclust.ensemble.pk_clustering import PkClustering


class TestEnsembleGraph:
    def test_ensemble_clustering(self, data_json):
        pk_clustering = PkClustering(data_json)
        pk_clustering.run(run_semi_supervised=False, grid_search=True)

        ensemble_graph = EnsembleGraph(pk_clustering)
        ensemble_graph.ensemble_clustering()
        print(ensemble_graph.ensemble_results.communities)
        assert True
