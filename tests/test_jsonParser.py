import pytest
import networkx as nx

from aleclust.processing.preprocessing import path_to_json
from aleclust.processing.paohJsonParser import PaohJsonParser
from aleclust.processing.parameters import Parameters


class TestPaohJsonParser:
    @pytest.mark.parametrize("projection", [True, False])
    def test_own_json_to_graph(self, data_json, projection):
        json_parser = PaohJsonParser(data_json)
        parameters = json_parser.parameters

        parameters.projection = projection
        parameters.parse_preprocessing_parameters()

        graph = json_parser.own_json_to_graph()
        if projection:
            assert graph.graph["bipartite"] is False
            assert json_parser.parameters.bipartite is False
        else:
            assert graph.graph["bipartite"] is True
            assert json_parser.parameters.bipartite is True

        assert parameters.source_entity_type == data_json["metadata"]["source_entity_type"]
        assert parameters.target_entity_type == data_json["metadata"]["target_entity_type"]

