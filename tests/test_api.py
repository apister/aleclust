import pytest
from flask import Flask, jsonify, request, send_file

from aleclust.globals import UNIPARTITE_CDA, BIPARTITE_CDA
from api import static_clustering_request


def test_app_base(app, client):
    res = client.get('/')
    assert res.status_code == 200


@pytest.mark.parametrize("clustering_algo", UNIPARTITE_CDA.keys())
def test_clustering_unipartite(app, client, data_json, clustering_algo):
    res = client.post('/clustering/' + clustering_algo, json=data_json)
    assert res.status_code == 200


@pytest.mark.parametrize("clustering_algo", UNIPARTITE_CDA.keys())
def test_static_clustering_request(app, clustering_algo, data_json):
    with app.test_request_context(json=data_json) as flask_request:
        json_communities = static_clustering_request(request, clustering_algo)

    assert True
