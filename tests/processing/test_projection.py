import pytest

import aleclust.processing.projection as projection


def test_weighted_projection(medium_data_json):
    source_type = medium_data_json["metadata"]["source_entity_type"]
    target_type = medium_data_json["metadata"]["target_entity_type"]

    graph_uni_json = projection.weighted_projection(medium_data_json, return_json=True)

    assert len([node for node in medium_data_json["nodes"] if node["entity_type"] == target_type]) == len(
        graph_uni_json["nodes"])
