import pathlib

import pytest
import rapidjson

from aleclust.processing.paohJsonParser import PaohJsonParser
from api import app as flask_app

this_file_path = str(pathlib.Path(__file__).parent.absolute())

data_path_2_1_0 = this_file_path + "/../data/new_json/v2.1.0/"
infovis_2009_2018_fp = data_path_2_1_0 + "INFOVIS-2009-2018.json"
vast_2009_2018_fp = data_path_2_1_0 + "VAST-2009-2018.json"
marie_boucher_fp = data_path_2_1_0 + "Marie_Boucher.json"
marie_boucher_small_fp = data_path_2_1_0 + "Marie_Boucher_small.json"
poutine_fp = data_path_2_1_0 + "Poutine_Vitalina.json"
poutine_fp_paoh = data_path_2_1_0 + "Poutine_Vitalina_fromPaohvis.json"

data_path_2_0 = this_file_path + "/../data/new_json/v2/"
nicoletta_fp = data_path_2_0 + "Nicoletta_data.json"
test_pk_fp = data_path_2_0 + "test_prior_knowledge2.json"


# This class allows storing data across tests
class StorageTest:
    graph_MB_small = None


@pytest.fixture(scope="session", params=[marie_boucher_small_fp, test_pk_fp])
def graph_unipartite(request):
    json = rapidjson.loads(open(request.param, "r").read())
    parser = PaohJsonParser(json)
    parameters = parser.parameters
    parameters.projection = True
    parameters.parse_preprocessing_parameters()

    graph = parser.own_json_to_graph()
    return graph, parameters


@pytest.fixture(scope="session", params=[marie_boucher_small_fp, test_pk_fp])
def graph_bipartite(request):
    json = rapidjson.loads(open(request.param, "r").read())
    parser = PaohJsonParser(json)
    parameters = parser.parameters
    parameters.projection = False
    parameters.parse_preprocessing_parameters()

    graph = parser.own_json_to_graph()
    return graph, parameters


@pytest.fixture(scope="session")
def russian_graph():
    json = rapidjson.loads(open(poutine_fp_paoh, "r").read())
    parser = PaohJsonParser(json)
    parser.parameters.projection = True
    parser.parameters.parse_preprocessing_parameters()
    graph = parser.own_json_to_graph()

    # for n in graph.nodes():
    #     del G.nodes[n]["value"]
    #     del G.nodes[n]["value"]

    return graph, parser.parameters


def test_diff_data_paoh():
    json = rapidjson.loads(open(poutine_fp, "r").read())
    parser = PaohJsonParser(json)
    parser.parameters.parse_preprocessing_parameters()
    graph = parser.own_json_to_graph()

    json2 = rapidjson.loads(open(poutine_fp_paoh, "r").read())
    parser2 = PaohJsonParser(json2)
    parser2.parameters.parse_preprocessing_parameters()
    graph_paoh = parser2.own_json_to_graph()

    print(graph.nodes.data())
    print(graph.edges.data())

    print(graph_paoh.nodes.data())
    print(graph_paoh.edges.data())

    assert len(graph.nodes) == len(graph_paoh.nodes)
    assert len(graph.edges) == len(graph_paoh.edges)

    for node, attrs in graph.nodes(data=True):
        if "DOM" in graph.nodes[node]:
            assert graph.nodes[node]["DOM"] == graph_paoh.nodes[node]["DOM"]
        if "SEXE" in graph.nodes[node]:
            assert graph.nodes[node]["SEXE"] == graph_paoh.nodes[node]["SEXE"]

    for edge in graph_paoh.edges():
        assert edge in graph.edges()

    for edge in graph.edges():
        assert edge in graph_paoh.edges()

    import networkx as nx
    assert nx.is_isomorphic(graph, graph_paoh)


@pytest.fixture
def app():
    yield flask_app


@pytest.fixture
def client(app):
    return app.test_client()
