import networkx as nx
import pytest

from aleclust.globals import UNIPARTITE_CDA, BIPARTITE_CDA, ATTRIBUTE_UNIPARTITE_CDA
from aleclust.clustering.clustering import applyClusteringAlgorithm
from aleclust.clustering.attributeCD import attribute_clustering_preprocessing
from aleclust.processing.parameters import Parameters
from aleclust.clustering.communities import Communities


@pytest.mark.parametrize("clustering_algo", UNIPARTITE_CDA.keys())
def test_apply_clustering_algorithm_unipartite(graph_unipartite, clustering_algo):
    graph, parameters = graph_unipartite[0], graph_unipartite[1]
    assert type(graph) == nx.Graph
    assert type(parameters) == Parameters

    parameters.set_default_clustering_parameters(clustering_algo)
    partition = applyClusteringAlgorithm(graph, clustering_algo, **parameters.clustering_parameters)
    assert type(partition) == Communities
    assert len(graph.nodes) == len([node for com in partition.communities for node in com])


@pytest.mark.parametrize("clustering_algo", BIPARTITE_CDA.keys())
def test_apply_clustering_algorithm_bipartite(graph_bipartite, clustering_algo):
    graph, parameters = graph_bipartite[0], graph_bipartite[1]
    parameters.set_default_clustering_parameters(clustering_algo)
    partition = applyClusteringAlgorithm(graph, clustering_algo, **parameters.clustering_parameters)

    assert type(partition) == Communities
    assert len([n for n, attr in graph.nodes.data() if attr['entity_type'] == parameters.target_entity_type]) == len([
        node
        for com in
        partition.communities
        for
        node in com])


def test_apply_clustering_algorithm_attribute_eva(russian_graph):
    graph, parameters = russian_graph[0], russian_graph[1]
    parameters.set_default_clustering_parameters("eva")

    attributes = parameters.communities
    print(attributes)
    parameters.clustering_parameters["attributes"] = attributes

    print(parameters.clustering_parameters)
    partition = applyClusteringAlgorithm(graph, "eva", **parameters.clustering_parameters)
    print(partition)


    # assert type(partition) == Communities
    # assert len([n for n, attr in graph.nodes.data() if attr['entity_type'] == parameters.target_entity_type]) == len([
    #     node
    #     for com in
    #     partition.communities
    #     for
    #     node in com])


# def test_apply_clustering_algorithm_attribute(russian_graph):
#     graph, parameters = russian_graph[0], russian_graph[1]
#
#     # for attribute in ["SEXE", "POUTINE", "AGE2019"]
#     # graph2 = graph.copy()
#     # for node in graph2.nodes():
#     #     for attr in graph2.nodes[node]:
#     #         if attr not in ["SEXE"]:
#     #             del graph.nodes[node][attr]
#
#     # print(graph.nodes[13])
#     # graph.remove_node(13)
#     for n in graph.nodes():
#         del graph.nodes[n]["value"]
#
#     for attribute in ["SEXE"]:
#         print("AA", graph.nodes.data())
#
#         parameters.clustering_parameters["attributes"] = [attribute]
#         print(parameters.clustering_parameters)
#         partition = applyClusteringAlgorithm(graph, "eva", **parameters.clustering_parameters)
#         print(partition.communities)
#
#     # for attribute in ["POUTINE", "AGE2019"]:
#     #     parameters.clustering_parameters["attributes"] = [attribute]
#     #     partition = applyClusteringAlgorithm(graph, "ilouvain", **parameters.clustering_parameters)
#     #     print(partition.communities)
#     assert True
