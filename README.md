# How it works

aleClust is a python graph clustering package, connecting algorithms implementations from several libraries (NetworkX, CDLIB, Scikit-Learn...) and introducing new ones. It implements as well an ensemble clustering scheme, and the backend part of the PK-Clustering project (https://www.aviz.fr/Research/pkclustering)

# Installing

Connect to the directory `aleclust`.

Create a Python conda environment:
```
conda activate aleclust
```

Install the dynCom package:

``` shell
cd dynCom
pip install .
```

Install the aleclust package:

``` shell
cd ..
pip install .
```

Run the server:

``` shell
python api.py
```

# PK-Clustering

aleClust was made for the PK-Clustering project. To launch it, you have to first launch the aleClust api on your computer as described above.
Then, you have to install and execute PAOHVis, which is the front-end of the project. See https://gitlab.inria.fr/aviz/paovis and https://www.aviz.fr/Research/Paohvis 


# Authors

Alexis Pister, Paolo Buono, Jean-Daniel Fekete, Catherine Plaisant, Paola Valdivia

